/**
 * Created by Utilisateur on 08/06/2016.
 */

$(document).ready(function () {
    $('.btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.btn-minimize').click(function (e) {
        e.preventDefault();
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible')) $('i', $(this)).removeClass('chevron-up').addClass('chevron-down');
        else                       $('i', $(this)).removeClass('chevron-down').addClass('chevron-up');
        $target.slideToggle();
    });
    $('.box-content').on('click', '.btn-minimize-tree', function (e) {
        e.preventDefault();
        var $target = $(this).next('.tree-category');
        if ($target.is(':visible')) $('i', $(this)).removeClass('chevron-up').addClass('chevron-down');
        else                       $('i', $(this)).removeClass('chevron-down').addClass('chevron-up');
        $target.slideToggle();
    });
    $(".btn-setting").click(function (e) {
        e.preventDefault();
        $('#myModal').modal('show');
    });

    $("#arrow-balise, #arrow-chariot").click(function () {
        switch ($(this).attr('id')) {
            case "arrow-balise":
                $(".panel-balise, .panel-balise-remonte").hide("slide", {direction: "right"}, 500, function () {
                    $(".panel-chariot, .panel-chariot-remonte").show("slide", {direction: "left"}, 500);
                });
                $(".map-box-blue").addClass("map-box-green");
                $(".map-box-green").removeClass("map-box-blue");
                $(".search-box-blue").addClass("search-box-green");
                $(".search-box-green").removeClass("search-box-blue");
                break;
            case "arrow-chariot":
                $(".panel-chariot, .panel-chariot-remonte").hide("slide", {direction: "left"}, 500, function () {
                    $(".panel-balise, .panel-balise-remonte").show("slide", {direction: "right"}, 500);
                });
                $(".map-box-green").addClass("map-box-blue");
                $(".map-box-blue").removeClass("map-box-green");
                $(".search-box-green").addClass("search-box-blue");
                $(".search-box-blue").removeClass("search-box-green");
                break;
        }
    });

});