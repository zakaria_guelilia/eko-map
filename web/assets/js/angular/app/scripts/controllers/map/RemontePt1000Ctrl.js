/**
 * Created by Utilisateur on 30/06/2016.
 */

angular.module('EkolisApp')
    .controller('RemontePt1000Ctrl', ['$scope', 'getFactory', 'shareFactory', function ($scope, getFactory, share) {
        var _pt1000List = [];
        var _pt1000Filter = [];
        var _ids = [];

        $scope.pt1000 = {
            pt1000List: function (val) {
                return arguments.length ? (_pt1000List = val) : _pt1000List;
            },
            pt1000Filter: function (val) {
                return arguments.length ? (_pt1000Filter = val) : _pt1000Filter;
            },
            ids: function (val) {
                return arguments.length ? (_ids = val) : _ids;
            }
        };
        $scope.tempData = {};
        $scope.tempPoint = {};
        $scope.pt1000Loading = false;

        $scope.$watch(function () {
                return share._filter;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) $scope.pt1000.pt1000Filter(newValue);
            }, true);
        
        $scope.$watch(function () {
                return share._search;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) {
                    $scope.pt1000.pt1000List([]);
                    fetchData('search');
                }
            }, true);

        $scope.$watch(function () {
                return share._baliseList;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) {
                    var key = Object.keys(newValue)[0];
                    switch (key) {
                        case 'add':
                            //$scope.pt1000.pt1000List(newValue['add']);
                            angular.forEach(newValue['add'], function (value) {
                                this.push(value.id)
                            }, $scope.pt1000.ids());
                            fetchData();
                            break;
                        case 'remove':
                            angular.forEach(newValue['remove'], function (value) {
                                var index = $scope.pt1000.ids().indexOf(value.id);
                                if (index > -1) $scope.pt1000.ids().splice(index, 1);
                                angular.forEach($scope.pt1000.pt1000List(), function (value2) {
                                    if (value2.id == value.id) {
                                        $scope.pt1000.pt1000List().splice($scope.pt1000.pt1000List().indexOf(value2), 1);
                                    }
                                });
                            });
                            $scope.tempData = {'remove': newValue['remove']};
                            angular.element('.loading-temp').hide();
                            break;
                    }
                }
            }, true);

        function fetchData(arg1) {
            var arr = $scope.pt1000.pt1000Filter();
            getFactory.getPt1000DataByBeacon($scope.pt1000.ids(), arr[0], arr[1]).query(function (data) {
                if (data !== null && data !== 'undefined' && data != '') {
                    if (data[0].time != '1970-01-01 00:00:00') {
                        angular.forEach(data, function (value) {
                            console.log(value.deviceId);
                            var obj = {'id': value.deviceId, 'name': value.name};
                            if (findWithAttr($scope.pt1000.pt1000List(), 'id', value.deviceId) == null) {
                                $scope.pt1000.pt1000List().push(obj)
                            }
                            //if ($scope.pt1000.pt1000List().indexOf(obj) === -1) $scope.pt1000.pt1000List().push(obj);
                        });
                        console.log("list: " + JSON.stringify(_pt1000List));
                        if (arg1 == 'search') data[0]['search'] = true;
                        $scope.tempData = data;
                    }
                }
                angular.element('.loading-temp').hide();
            });
        }

        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
        }

    }]);