/**
 * Created by Utilisateur on 12/07/2016.
 */

angular.module('EkolisApp')
    .controller('RemonteTpmsCtrl', ['$scope', 'getFactory', 'shareFactory', function ($scope, getFactory, share) {
        var _tpmsList = [];
        var _tpmsFilter = [];
        var _ids = [];

        $scope.tpms = {
            tpmsList: function (val) {
                return arguments.length ? (_tpmsList = val) : _tpmsList;
            },
            tpmsFilter: function (val) {
                return arguments.length ? (_tpmsFilter = val) : _tpmsFilter;
            },
            ids: function (val) {
                return arguments.length ? (_ids = val) : _ids;
            }
        };
        $scope.tpmsData = {};
        $scope.tpmsPoint = {};
        $scope.tpmsLoading = false;

        $scope.$watch(function () {
                return share._filter;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) $scope.tpms.tpmsFilter(newValue);
            }, true);

        $scope.$watch(function () {
                return share._search;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) {
                    $scope.tpms.tpmsList([]);
                    fetchData('search');
                }
            }, true);

        $scope.$watch(function () {
                return share._baliseList;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) {
                    var key = Object.keys(newValue)[0];
                    switch (key) {
                        case 'add':
                            //$scope.pt1000.pt1000List(newValue['add']);
                            angular.forEach(newValue['add'], function (value) {
                                this.push(value.id)
                            }, $scope.tpms.ids());
                            fetchData();
                            break;
                        case 'remove':
                            angular.forEach(newValue['remove'], function (value) {
                                var index = $scope.tpms.ids().indexOf(value.id);
                                if (index > -1) $scope.tpms.ids().splice(index, 1);
                                angular.forEach($scope.tpms.tpmsList(), function (value2) {
                                    if (value2.id == value.id) {
                                        $scope.tpms.tpmsList().splice($scope.tpms.tpmsList().indexOf(value2), 1);
                                    }
                                });
                            });
                            $scope.tpmsData = {'remove': newValue['remove']};
                            angular.element('.loading-temp').hide();
                            break;
                    }
                }
            }, true);

        function fetchData(arg1) {
            var arr = $scope.tpms.tpmsFilter();
            getFactory.getTpmsDataByBeacon($scope.tpms.ids(), arr[0], arr[1]).query(function (data) {
                if (data !== null && data !== 'undefined' && data != '') {
                    if (data[0].time != '1970-01-01 00:00:00') {
                        angular.forEach(data, function (value) {
                            console.log(value.deviceId);
                            var obj = {'id': value.deviceId, 'name': value.name};
                            if (findWithAttr($scope.tpms.tpmsList(), 'id', value.deviceId) == null) {
                                $scope.tpms.tpmsList().push(obj)
                            }
                            //if ($scope.pt1000.pt1000List().indexOf(obj) === -1) $scope.pt1000.pt1000List().push(obj);
                        });
                        console.log("list: " + JSON.stringify(_tpmsList));
                        if (arg1 == 'search') data[0]['search'] = true;
                        $scope.tpmsData = data;
                        console.log(data)
                    }
                }
                angular.element('.loading-temp').hide();
            });
        }

        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
        }

    }]);