/**
 * Created by Utilisateur on 2016-06-03.
 */


angular.module('EkolisApp')
    .controller('RemonteCtrl', ['$scope', 'shareFactory', 'getFactory', function ($scope, share, getFactory) {
        var _baliseHash = {};
        var _chariotHash = {};
        var _mode = 'balise';
        var _chariotId = '';
        var _remonteFilter = [];
        $scope.map = {
            baliseHash: function () {
                return _baliseHash;
            },
            chariotHash: function () {
                return _chariotHash;
            },
            mode: function (val) {
                return arguments.length ? (_mode = val) : _mode;
            },
            chariotId: function (val) {
                return arguments.length ? (_chariotId = val) : _chariotId;
            },
            remonteFilter: function (val) {
                return arguments.length ? (_remonteFilter = val) : _remonteFilter;
            }
        };
        $scope.points = {};

        $scope.$watch(function () {
                return share._search;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) {
                    getFactory.abort();
                    var arr = $scope.map.remonteFilter();
                    switch ($scope.map.mode()) {
                        case 'balise':
                            var tab = [];
                            angular.forEach($scope.map.baliseHash(), function (value, key) {
                                tab.push(key);
                            });
                            if (tab.length > 0) {
                                angular.element("#loader").show("slide", {direction: "up"}, 500);
                                getFactory.getRemonteByBeacon(tab, arr[0], arr[1]).query(function (data) {
                                    console.log("data " + data);
                                    if (data !== null && data != '') {
                                        if (data[0] !== 'undefined') {
                                            data[0]['newSearch'] = true;
                                        }
                                    }
                                    $scope.points = data;
                                    angular.element('.loading').hide();
                                });
                            }
                            break;
                        case 'chariot':
                            angular.forEach($scope.map.chariotHash(), function (value, key) {
                                getFactory.getRemonteChariot(key, arr[0], arr[1]).query(function (data) {
                                    data[0]['newSearch'] = true;
                                    data[0]['chariotId'] = '6f15f40000000202';
                                    facto('chariot', $scope.map.chariotHash(), data, key);
                                })
                            });
                            break;
                    }
                }
            }, true);

        $scope.setMode = function () {
            switch ($scope.map.mode()) {
                case 'balise':
                    $scope.map.mode('chariot');
                    break;
                case 'chariot':
                    $scope.map.mode('balise');
                    break;
            }
        };

        $scope.$watch(function () {
                return share._filter;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) $scope.map.remonteFilter(newValue);
            }, true);

        $scope.$watch(function () {
                return share._baliseList;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) pushToMap(newValue, 'balise');
            }, true);

        $scope.$watch(function () {
                return share.actionChariot;
            },
            function (newValue, oldValue) {
                //noinspection JSValidateTypes
                if (newValue !== oldValue) pushToMap(newValue, 'chariot');
            }, true);

        function pushToMap(val, t, r) {
            var key = Object.keys(val)[0];
            var d = val[key];
            var ids = [];
            angular.forEach(d, function (value) {
                ids.push(value.id);
            });
            var arr = $scope.map.remonteFilter();
            var l;
            switch (t) {
                case 'balise':
                    l = $scope.map.baliseHash();
                    break;
                case 'chariot':
                    l = $scope.map.chariotHash();
                    break;
            }
            console.log("RemonteCtrl watch: " + JSON.stringify(val));
            switch (key) {
                case 'add':
                    switch (t) {
                        case 'balise':
                            console.log("d " + ids);
                            $("#loader").show("slide", {direction: "up"}, 500);
                            getFactory.getRemonteByBeacon(ids, arr[0], arr[1]).query(function (data) {
                                angular.forEach(ids, function (value) {
                                    l[value] = data;
                                });
                                angular.element('.loading').hide();
                                $scope.points = data;
                            });
                            break;
                        case 'chariot':
                            angular.forEach(ids, function (value) {
                                getFactory.getRemonteChariot(value, arr[0], arr[1]).query(function (data) {
                                    facto(t, l, data, value);
                                })
                            });
                    }
                    break;
                case 'remove':
                    angular.forEach(ids, function (value) {
                        delete l[value];
                    });
                    $scope.points = {"toDelete": ids};
                    angular.element('.loading').hide();
                    break;
                case 'softAdd':
                    angular.forEach(ids, function (value) {
                        l[value] = '';
                    });
                    break;
                case 'softRemove':
                    angular.forEach(ids, function (value) {
                        delete l[value];
                    });
                    break;
            }
        };

        function facto(t, l, data, value) {
            angular.element('.loading').hide();
            if (typeof data[0] != 'undefined') {
                if (data[0].time != '1970-01-01 00:00:00') {
                    if (t == 'chariot') data[0]['chariotId'] = value;
                    l[value] = data;
                    $scope.points = data;
                } else {
                    var tab = [value];
                    $scope.points = {"toDelete": tab};
                }
            } else {
                $scope.points = {"toDelete": [value]};
            }
        }
    }]);
