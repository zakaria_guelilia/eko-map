/**
 * Created by Utilisateur on 03/06/2016.
 */

angular.module('EkolisApp')
    .controller('BaliseCtrl', ['$scope', 'getFactory', 'shareFactory', function ($scope, getFactory, share) {
        var _id = 0;
        var _baliseList = {};
        var _chariotList = {};
        var _beaconSearchList = [];
        var _beaconSelected = '';
        $scope.balise = {
            id: function (newId) {
                return arguments.length ? (_id = newId) : _id;
            },
            baliseList: function (newList) {
                return arguments.length ? (_baliseList = newList) : _baliseList;
            },
            beaconSearchList: function (newList) {
                return arguments.length ? (_beaconSearchList = newList) : _beaconSearchList;
            },
            beaconSelected: function (newList) {
                if(arguments.length){
                    _beaconSelected = newList;
                    if($.inArray(_beaconSelected, _beaconSearchList) != -1) checkSearch(_beaconSelected);
                    return true;
                }else{
                    return _beaconSelected;
                }
            }
        };

        $scope.chariot = {
            id: function (newId) {
                return arguments.length ? (_id = newId) : _id;
            },
            chariotList: function (newList) {
                return arguments.length ? (_chariotList = newList) : _chariotList;
            }
        };

        $scope.fetch = function (id) {
            var path;
            switch (id) {
                case 'super_admin':
                    path = getFactory.getBeacon();
                    break;
                default:
                    path = getFactory.getBeaconByFleet(id);
                    break;
            }
            path.query(function (data) {
                $scope.balise.baliseList({});
                angular.forEach(data, function (value) {
                    if (!(value.fleet in $scope.balise.baliseList())) {
                        $scope.balise.baliseList()[value.fleet] = {};
                        $scope.balise.baliseList()[value.fleet]['data'] = {};
                        $scope.balise.baliseList()[value.fleet]['checkbox'] = false;
                    }
                    if (value.subFleet == null) value.subFleet = 'sous-flotte';
                    if (!(value.subFleet in $scope.balise.baliseList()[value.fleet]['data'])) {
                        $scope.balise.baliseList()[value.fleet]['child'] += 1;
                        $scope.balise.baliseList()[value.fleet]['data'][value.subFleet] = {};
                        $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['data'] = {};
                        $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['checkbox'] = false;
                    }
                    $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['child'] += 1;
                    $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['data'][value.deviceId] = {};
                    $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['data'][value.deviceId]['deviceId'] = value.deviceId;
                    $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['data'][value.deviceId]['name'] = value.name;
                    $scope.balise.baliseList()[value.fleet]['data'][value.subFleet]['data'][value.deviceId]['checkbox'] = false;

                    $scope.balise.beaconSearchList().push(value.name);
                });
            });

            if (id == 'super_admin' || id == 'LE GUEVEL') {
                $scope.chariot.chariotList()['LE GUEVEL'] = {};
                $scope.chariot.chariotList()['LE GUEVEL']['data'] = {};
                $scope.chariot.chariotList()['LE GUEVEL']['checkbox'] = false;
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL'] = {};
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL']['data'] = {};
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL']['checkbox'] = false;
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL']['data']['6f15f40000000202'] = {};
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL']['data']['6f15f40000000202']['name'] = '6f15f40000000202';
                $scope.chariot.chariotList()['LE GUEVEL']['data']['LE GUEVEL']['data']['6f15f40000000202']['checkbox'] = false;
            }

        };

        $scope.baliseClick = function ($event, type) {
            share.load();
            var el = $scope.balise.baliseList();
            if (type == 'chariot') el = $scope.chariot.chariotList();
            baliseCheckbox($event, el);
            type == 'balise' ? showRemonteBalise($event) : showRemonteChariot($event);
        };

        function checkSearch(beacon){
            angular.forEach($scope.balise.baliseList(), function (value, key) {
                angular.forEach(value.data, function (value2, key2) {
                    angular.forEach(value2.data, function (value3, key3) {
                        if(value3.name == beacon){
                            value3.checkbox = true;
                            showRemonteBaliseSelected(value3);
                            baliseCheckboxChange($scope.balise.baliseList()[key], $scope.balise.baliseList()[key]['data'][key2], value3);

                        }
                    });
                });
            });
        }

        function baliseCheckbox($event, l) {
            var check = true;
            var id = angular.element($event.currentTarget).attr('id');
            if (id.indexOf('check_') == 0 || id.indexOf('chariot_check_') == 0) check = false;
            var attr = angular.element($event.currentTarget).attr('data-parents');
            var parents = attr.split('_');
            switch (parents.length) {
                case 1:
                    if (check) l[parents[0]]['checkbox'] = !l[parents[0]]['checkbox'];
                    baliseCheckboxChange(l[parents[0]]);
                    break;
                case 2:
                    if (check) l[parents[0]]['data'][parents[1]]['checkbox'] = !l[parents[0]]['data'][parents[1]]['checkbox'];
                    baliseCheckboxChange(l[parents[0]], l[parents[0]]['data'][parents[1]]);
                    break;
                case 3:
                    if (check) l[parents[0]]['data'][parents[1]]['data'][parents[2]]['checkbox'] = !l[parents[0]]['data'][parents[1]]['data'][parents[2]]['checkbox'];
                    baliseCheckboxChange(l[parents[0]],
                        l[parents[0]]['data'][parents[1]],
                        l[parents[0]]['data'][parents[1]]['data'][parents[2]]);
                    break;
            }
        }


        function baliseCheckboxChange(el1, el2, el3) {
            var check;
            switch (arguments.length) {
                case 1:
                    check = el1.checkbox;
                    angular.forEach(el1.data, function (value, key) {
                        angular.forEach(value.data, function (value2, key2) {
                            value2.checkbox = check;
                        });
                        value.checkbox = check;
                    });
                    break;
                case 2:
                    check = el2.checkbox;
                    angular.forEach(el2.data, function (value, key) {
                        if (value.checkbox != check) value.checkbox = check;
                    });
                    checkParent(check, el1);
                    break;
                case 3:
                    check = el3.checkbox;
                    checkParent(check, el2);
                    checkParent(check, el1);
                    break;
            }
        }

        function checkParent(status, el) {
            var diff = false;
            angular.forEach(el.data, function (value, key) {
                if (value.checkbox != status) diff = true;
            });
            diff ? el.checkbox = false : el.checkbox = status;
        }

        function showRemonteBalise($event) {
            var id = angular.element($event.currentTarget).attr('id');
            if (id.indexOf('check_') == 0) id = id.replace('check_', '');
            var ids = id.split("_");
            showRemonte('balise', $scope.balise.baliseList(), id, ids, $event)
        }

        function showRemonteChariot($event) {
            var id = angular.element($event.currentTarget).attr('id');
            if (id.indexOf('chariot_check_') == 0) id = id.replace('chariot_check_', '');
            if (id.indexOf('chariot_') == 0) id = id.replace('chariot_', '');
            var ids = id.split("_");
            showRemonte('chariot', $scope.chariot.chariotList(), id, ids, $event)
        }

        function showRemonteBaliseSelected(beaconData) {
            console.log(beaconData);
            sendToFacto([{"id": beaconData.deviceId, "name": beaconData.name}], beaconData, 'balise');
        }

        function showRemonte(t, l, id, ids, $event) {
            var tab = [];

            if (id.indexOf('fleet') == 0) {
                angular.forEach(l[ids[1]]['data'], function (value, key) {
                    angular.forEach(value['data'], function (value2, key2) {
                        tab.push({"id": key2, "name": value2.name})
                    })
                });
                sendToFacto(tab, l[ids[1]], t);
            } else if (id.indexOf('subfleet') == 0) {
                angular.forEach(l[ids[1]]['data'][ids[2]]['data'], function (value, key) {
                    tab.push({"id": key, "name": value.name})
                });
                sendToFacto(tab, l[ids[1]]['data'][ids[2]], t);
            }
            else {
                var attr = angular.element($event.currentTarget).attr('data-parents');
                var split = attr.split('_');
                tab.push({"id": id, "name": l[split[0]]['data'][split[1]]['data'][split[2]].name});
                sendToFacto(tab, l[split[0]]['data'][split[1]]['data'][split[2]], t);
            }
        }

        function sendToFacto(tab, e, mode) {
            switch (mode) {
                case 'balise':
                    if (e.checkbox) {
                        share.loadBaliseInfo(tab)
                        /*share.addBaliseSet(tab);
                         share.showTemp(tab);*/
                    } else {
                        share.removeBaliseInfo(tab)
                        /*share.removeBaliseSet(tab);
                         share.removeTemp(tab);*/
                    }
                    break;
                case 'chariot':
                    e.checkbox ? share.addChariotSet(tab) : share.removeChariotSet(tab);
                    break;
            }
        }
    }]);