/**
 * Created by Utilisateur on 30/06/2016.
 */

angular.module('EkolisApp')
    .controller('MainCtrl', ['$scope', 'shareFactory', 'getFactory', function ($scope, share, getFactory) {

        var _filter = 'time';
        var _date = '';
        var _rangeBegin = '';
        var _rangeEnd = '';

        $scope.main = {
            filter: function (val) {
                return arguments.length ? (_filter = val) : _filter;
            },
            date: function (val) {
                return arguments.length ? (_date = val) : _date;
            },
            rangeBegin: function (val) {
                return arguments.length ? (_rangeBegin = val) : _rangeBegin;
            },
            rangeEnd: function (val) {
                return arguments.length ? (_rangeEnd = val) : _rangeEnd;
            }
        };

        $scope.filter = function (val) {
            $scope.main.filter(val);
            setFilter();
        };

        $scope.dateChanged = function (date) {
            $scope.main.date(convertDate(date));
            setFilter();
        };

        $scope.dateBeginChanged = function (date) {
            $scope.main.rangeBegin(convertDate(date));
            setFilter();
        };

        $scope.dateEndChanged = function (date) {
            $scope.main.rangeEnd(convertDate(date));
            setFilter();
        };

        $scope.mainSearch = function () {
            share.load();
            share.search();
        };

        function setFilter() {
            var val = $scope.main.filter();
            var val2 = null;
            switch (val) {
                case 'date':
                    val = $scope.main.date();
                    break;
                case 'link':
                    val = $scope.main.rangeBegin();
                    val2 = $scope.main.rangeEnd();
                    break;
            }
            share.filter([val, val2]);
        }

        function convertDate(date) {
            var split1 = date.split(' ');
            var split2 = split1[0].split('-');
            return split2[2] + "-" + split2[1] + "-" + split2[0] + " " + split1[1] + ":00";
        }


    }]);