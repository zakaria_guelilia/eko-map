/**
 * Created by Utilisateur on 07/07/2016.
 */

angular.module('EkolisApp')
    .controller('FleetCtrl', ['$scope', 'getFactory', '$uibModal', '$location', function ($scope, getFactory, $uibModal) {
        var _fleetList = {};
        $scope.fleets = {
            fleetList: function (newList) {
                return arguments.length ? (_fleetList = newList) : _fleetList;
            }
        };
        $scope.fetchFleets = function () {
            getFactory.getFleets().query(function (data) {
                angular.forEach(data, function (value) {
                    $scope.fleets.fleetList()[value.fleet] = value;
                });
                console.log($scope.fleets.fleetList());
            });
        };


        $scope.fleetCreate = function (val) {
            $uibModal.open({
                animation: true,
                templateUrl: 'fleetModalContent.html',
                controller: 'fleetModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return val;
                    }
                }
            });
        };

        $scope.fleetDelete = function (id) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userConfirmModal.html',
                controller: 'fleetModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            })
                .result.then(function () {
                getFactory.deleteFleet(id).then(function (response) {
                    if(response.data[0]['msg'] == "success") delete $scope.fleets.fleetList()[id];
                });
            });


        }
    }])
    .controller('fleetModalInstanceCtrl', function ($scope, $uibModalInstance, items) {
        $scope.labelDeleteForm = 'la flotte';
        $scope.valIdForm = '';
        $scope.valFleetForm = '';

        $scope.fleetForm = {
            valIdForm: function (val) {
                return arguments.length ? ( $scope.valIdForm = val) : $scope.valIdForm;
            },
            valFleetForm: function (val) {
                return arguments.length ? ($scope.valFleetForm = val) : $scope.valFleetForm;
            }
        };

        if (items) {
            if (typeof items === 'object') {
                $scope.fleetForm.valIdForm(items.id);
                $scope.fleetForm.valFleetForm(items.fleet);
            } else $scope.fleetForm.valIdForm(items);
        }

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });