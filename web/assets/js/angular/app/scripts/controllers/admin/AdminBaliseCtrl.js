/**
 * Created by Utilisateur on 07/07/2016.
 */

angular.module('EkolisApp')
    .controller('AdminBaliseCtrl', ['$scope', 'getFactory', '$uibModal', '$location', function ($scope, getFactory, $uibModal, $location) {
        var _baliseList = {};
        var _baliseFleetList = {};
        var _baliseSubFleetList = {};
        $scope.balises = {
            baliseList: function (newList) {
                return arguments.length ? (_baliseList = newList) : _baliseList;
            },
            baliseFleetList: function (newList) {
                return arguments.length ? (_baliseFleetList = newList) : _baliseFleetList;
            },
            baliseSubFleetList: function (newList) {
                return arguments.length ? (_baliseSubFleetList = newList) : _baliseSubFleetList;
            }
        };
        $scope.fetchBalises = function () {
            getFactory.getBeacon().query(function (data) {
                angular.forEach(data, function (value) {
                    $scope.balises.baliseList()[value.deviceId] = value;
                });
            });

            getFactory.getSubFleets().query(function (data) {
                angular.forEach(data, function (value) {
                    if (!(value.fleet in $scope.balises.baliseFleetList())) $scope.balises.baliseFleetList()[value.fleet] = [];
                    $scope.balises.baliseFleetList()[value.fleet].push(value.subFleet);
                    if (!(value.subFleet in $scope.balises.baliseSubFleetList())) $scope.balises.baliseSubFleetList()[value.subFleet] = {'show': false};
                });
            });
        };


        $scope.open = function (val) {
            $uibModal.open({
                animation: true,
                templateUrl: 'baliseModalContent.html',
                controller: 'baliseModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return val;
                    },
                    fleetModel: function () {
                        return $scope.balises.baliseFleetList();
                    },
                    subFleetmodel: function () {
                        return $scope.balises.baliseSubFleetList();
                    }
                }
            });
        };

        $scope.baliseDelete = function (id) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userConfirmModal.html',
                controller: 'baliseModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return id;
                    },
                    fleetModel: function () {
                        return '';
                    },
                    subFleetmodel: function () {
                        return '';
                    }
                }
            })
                .result.then(function () {
                getFactory.deleteBeacon(id).then(function (response) {
                    if(response.data[0]['msg'] == "success") delete $scope.balises.baliseList()[id];
                });
            });
        }
    }])
    .controller('baliseModalInstanceCtrl', function ($scope, $uibModalInstance, items, fleetModel, subFleetmodel) {
        $scope.labelDeleteForm = 'la balise';
        $scope.valIdForm = '';
        $scope.valdeviceIdForm = '';
        $scope.valNameForm = '';
        $scope.valSubFleetForm = '';
        $scope.valFleetForm = '';
        $scope.valFleetModel = fleetModel;
        $scope.valSubFleetModel = subFleetmodel;


        $scope.$watch('valFleetForm', function (newValue, oldValue) {
            console.log(oldValue + " " +  newValue)
            if((newValue === '' && oldValue === '') || newValue !== oldValue) $scope.baliseForm.valSubFleetForm('');
            angular.forEach($scope.baliseForm.valFleetModel()[oldValue], function (value) {
                $scope.baliseForm.valSubFleetModel()[value]['show'] = false;
            });
            angular.forEach($scope.baliseForm.valFleetModel()[newValue], function (value) {
                $scope.baliseForm.valSubFleetModel()[value]['show'] = true;
            });
        }, true);

        $scope.baliseForm = {
            valIdForm: function (val) {
                return arguments.length ? ( $scope.valIdForm = val) : $scope.valIdForm;
            },
            valdeviceIdForm: function (val) {
                return arguments.length ? ( $scope.valdeviceIdForm = val) : $scope.valdeviceIdForm;
            },
            valNameForm: function (val) {
                return arguments.length ? ( $scope.valNameForm = val) : $scope.valNameForm;
            },
            valFleetForm: function (val) {
                return arguments.length ? ($scope.valFleetForm = val) : $scope.valFleetForm;
            },
            valSubFleetForm: function (val) {
                return arguments.length ? ($scope.valSubFleetForm = val) : $scope.valSubFleetForm;
            },
            valSubFleetModel: function (val) {
                return arguments.length ? ($scope.valSubFleetModel = val) : $scope.valSubFleetModel;
            },
            valFleetModel: function (val) {
                return arguments.length ? ($scope.valFleetModel = val) : $scope.valFleetModel;
            }
        };

        if (items) {
            if (typeof items === 'object') {
                angular.forEach($scope.baliseForm.valSubFleetModel(), function (value) {
                    value.show = false;
                });
                $scope.baliseForm.valIdForm(items.deviceId);
                $scope.baliseForm.valdeviceIdForm(items.deviceId);
                $scope.baliseForm.valNameForm(items.name);
                $scope.baliseForm.valFleetForm(items.fleet);
                $scope.baliseForm.valSubFleetForm(items.subFleet);
            } else $scope.baliseForm.valIdForm(items);
        }

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });