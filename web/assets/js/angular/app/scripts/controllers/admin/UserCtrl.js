/**
 * Created by Utilisateur on 07/07/2016.
 */

angular.module('EkolisApp')
    .controller('UserCtrl', ['$scope', 'getFactory', '$uibModal', '$location', function ($scope, getFactory, $uibModal, $location) {
        var _userList = {};
        $scope.users = {
            userList: function (newList) {
                return arguments.length ? (_userList = newList) : _userList;
            }
        };
        $scope.fetchUsers = function () {
            getFactory.getUsers().query(function (data) {
                angular.forEach(data, function (value) {
                    var s = '';
                    angular.forEach(value.roles, function (value2) {
                        s += value2 + '/';
                    });
                    s = s.substring(0, s.length - 1);
                    value.roles = s;
                    $scope.users.userList()[value.id] = value;
                });
                console.log("az: " + JSON.stringify($scope.users.userList()));
            });
        };


        $scope.open = function (val) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userModalContent.html',
                controller: 'userModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return val;
                    }
                }
            });
        };

        $scope.userUpdate = function (url, id) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userModalContent.html',
                controller: 'userModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            })
        };

        $scope.userDelete = function (id) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userConfirmModal.html',
                controller: 'userModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            })
                .result.then(function () {
                getFactory.deleteUser(id).then(function (response) {
                    if(response.data[0]['msg'] == "success") delete $scope.users.userList()[id];
                });
            });


        }
    }])
    .controller('userModalInstanceCtrl', function ($scope, $uibModalInstance, items) {

        $scope.labelDeleteForm = "l'utilisateur";
        $scope.valIdForm = '';
        $scope.valUsernameForm = '';
        $scope.valPasswordForm = '';
        $scope.valEmailForm = '';
        $scope.valIsActiveForm = '1';
        $scope.valRoleForm = 'ROLE_USER';
        $scope.valFleetForm = '';

        $scope.userForm = {
            valIdForm: function (val) {
                return arguments.length ? ( $scope.valIdForm = val) : $scope.valIdForm;
            },
            valUsernameForm: function (val) {
                return arguments.length ? ($scope.valUsernameForm = val) : $scope.valUsernameForm;
            },
            valPasswordForm: function (val) {
                return arguments.length ? ($scope.valPasswordForm = val) : $scope.valPasswordForm;
            },
            valEmailForm: function (val) {
                return arguments.length ? ($scope.valEmailForm = val) : $scope.valEmailForm;
            },
            valIsActiveForm: function (val) {
                return arguments.length ? ($scope.valIsActiveForm = val) : $scope.valIsActiveForm;
            },
            valRoleForm: function (val) {
                return arguments.length ? ($scope.valRoleForm = val) : $scope.valRoleForm;
            },
            valFleetForm: function (val) {
                return arguments.length ? ($scope.valFleetForm = val) : $scope.valFleetForm;
            }
        };
        if (items) {
            if (typeof items === 'object') {
                $scope.userForm.valIdForm(items.id);
                $scope.userForm.valUsernameForm(items.username);
                $scope.userForm.valPasswordForm(items.password);
                $scope.userForm.valEmailForm(items.email);
                items.isActive ? $scope.userForm.valIsActiveForm('0') : $scope.userForm.valIsActiveForm('1');
                $scope.userForm.valRoleForm(items.roles);
                $scope.userForm.valFleetForm(items.fleet);
            } else $scope.userForm.valIdForm(items);
        }

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });