/**
 * Created by Utilisateur on 07/07/2016.
 */

angular.module('EkolisApp')
    .controller('SubFleetCtrl', ['$scope', 'getFactory', '$uibModal', '$location', function ($scope, getFactory, $uibModal, $location) {
        var _subFleetList = {};
        $scope.subFleets = {
            subFleetList: function (newList) {
                return arguments.length ? (_subFleetList = newList) : _subFleetList;
            }
        };
        $scope.fetchSubFleets = function () {
            getFactory.getSubFleets().query(function (data) {
                angular.forEach(data, function (value) {
                    $scope.subFleets.subFleetList()[value.subFleet] = value;
                });
                console.log($scope.subFleets.subFleetList());
            });
        };


        $scope.open = function (val) {
            $uibModal.open({
                animation: true,
                templateUrl: 'subFleetModalContent.html',
                controller: 'subFleetModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return val;
                    }
                }
            });
        };

        $scope.subFleetDelete = function (id) {
            $uibModal.open({
                animation: true,
                templateUrl: 'userConfirmModal.html',
                controller: 'subFleetModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            })
                .result.then(function () {
                getFactory.deleteSubFleet(id).then(function (response) {
                    if(response.data[0]['msg'] == "success") delete $scope.subFleets.subFleetList()[id];
                });
            });
        }
    }])
    .controller('subFleetModalInstanceCtrl', function ($scope, $uibModalInstance, items) {

        $scope.labelDeleteForm = 'la sous-flotte';
        $scope.valIdForm = '';
        $scope.valSubFleetForm = '';
        $scope.valFleetForm = '';

        $scope.subFleetForm = {
            valIdForm: function (val) {
                return arguments.length ? ( $scope.valIdForm = val) : $scope.valIdForm;
            },
            valSubFleetForm: function (val) {
                return arguments.length ? ($scope.valSubFleetForm = val) : $scope.valSubFleetForm;
            },
            valFleetForm: function (val) {
                return arguments.length ? ($scope.valFleetForm = val) : $scope.valFleetForm;
            }
        };
        if (items) {
            if (typeof items === 'object') {
                $scope.subFleetForm.valIdForm(items.subFleet);
                $scope.subFleetForm.valSubFleetForm(items.subFleet);
                $scope.subFleetForm.valFleetForm(items.fleet);
            } else $scope.subFleetForm.valIdForm(items);
        }

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });