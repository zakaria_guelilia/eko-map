/**
 * Created by Utilisateur on 03/06/2016.
 */

angular.module('EkolisApp')
    .service('getFactory', function ($resource, $q, $http) {
        //var url = 'http://ekolis.ddns.net/apiekolis/web/';
        //var url = 'http://localhost:8000/';
        var stopRequest = $q.defer();
        return {
            abort: function () {
                stopRequest.resolve();
                stopRequest = $q.defer()
            },
            getRemonteByBeacon: function (ids, filter, filter2) {
                return $resource('getRemonteByBeacon/', {}, {
                    query: {method: 'GET', params: {"balises[]": ids, "filter": filter, "filter2": filter2}, isArray: true}
                });
            },
            getRemonteChariot: function (id, filter, filter2) {
                return $resource('getRemonteByPt1000/' + id, {}, {
                    query: {method: 'GET', params: {"filter": filter, "filter2": filter2}, isArray: true}
                });
            },
            getAddress: function (lat, lon) {
                var deferred = $q.defer();

                $http.get('getAddress', {
                    params: {"lat": lat, "lon": lon},
                    async: true,
                    timeout: stopRequest.promise
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });

                return deferred.promise;
            },
            getPt1000DataByBeacon: function (ids, filter, filter2) {
                return $resource('getPt1000DataByBeacon/', {}, {
                    query: {method: 'GET', params: {"device_id[]": ids, "filter": filter, "filter2": filter2}, isArray: true}
                });
            },
            getTpmsDataByBeacon: function (ids, filter, filter2) {
                return $resource('getTpmsDataByBeacon/', {}, {
                    query: {method: 'GET', params: {"device_id[]": ids, "filter": filter, "filter2": filter2}, isArray: true}
                });
            },
            getBeaconByFleet: function (id) {
                return $resource('getBeaconByFleet/' + id, {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
            },
            getBeacon: function () {
                return $resource('getBeacon/', {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
            },
            deleteBeacon: function (val) {
                return $http({
                    url: '/admin/beacon/delete',
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: {id: val}
                });
            },
            getUsers: function () {
                return $resource('getUsers/', {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
            },
            deleteUser: function (val) {
                return $http({
                    url: '/admin/user/delete',
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: {id: val}
                });
            },
            getFleets: function () {
                return $resource('getFleets/', {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
            },
            deleteFleet: function (val) {
                return $http({
                    url: '/admin/fleet/delete',
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: {id: val}
                });

            },
            getSubFleets: function () {
                return $resource('getSubFleets/', {}, {
                    query: {method: 'GET', params: {}, isArray: true}
                });
            },
            deleteSubFleet: function (val) {
                return $http({
                    url: '/admin/subfleet/delete',
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: {id: val}
                });
            }
        }
    });
