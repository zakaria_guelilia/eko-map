/**
 * Created by Utilisateur on 06/06/2016.
 */

angular.module('EkolisApp')
    .factory('shareFactory', function ($q) {
        var deferred = $q.defer();
        var share = {
            actionChariot: {},
            _baliseList: {},
            _showTemp: {},
            _filter: [],
            _search: true,
            loadBaliseInfo: function (val) {
                share._baliseList = {'add': val};
            },
            removeBaliseInfo: function (val) {
                share._baliseList = {"remove": val}
            },
            getChariotHash: function () {
                return share.actionChariot;
            },
            addChariotSet: function (val) {
                share.actionChariot = {"add": val};
                console.log("share: " + JSON.stringify(share.actionChariot))
            },
            removeChariotSet: function (val) {
                share.actionChariot = {"remove": val};
                console.log("share: " + JSON.stringify(share.actionChariot))
            },
            filter: function (val) {
                share._filter = val;
            },
            search: function () {
                share._search = !share._search;
            },
            load: function () {
                var h = angular.element('#balise-list-group').height();
                var h2 = angular.element('.temp.content').height();
                angular.element('.loading').height(h);
                angular.element('.loading').show();

                angular.element('.loading-temp').height(h2);
                angular.element('.loading-temp').show();
            }
        };
        return share;
    });