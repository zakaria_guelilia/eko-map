/**
 * Created by Utilisateur on 07/07/2016.
 */

angular
    .module('EkolisApp', [
        'ngResource',
        'ngRoute',
        'ngAnimate',
        'ui.bootstrap'
    ])
    .config(function ($httpProvider, $interpolateProvider, $resourceProvider, $routeProvider) {
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
        $resourceProvider.defaults.cancellable = true;
    });