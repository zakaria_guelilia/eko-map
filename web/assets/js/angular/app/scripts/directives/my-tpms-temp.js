/**
 * Created by Utilisateur on 12/07/2016.
 */

angular.module('EkolisApp')
    .directive('myTpmsTemp', function () {

        return {
            restrict: 'A',
            replace: true,
            template: '<div></div>',
            link: function (scope, element, attr) {
                var collection = {};
                var picked = '';
                var plot = '';
                scope.$watch(attr.tpmsSource, function (data) {
                    if (!isEmpty(data)) {
                        if (!('remove' in data)) {
                            if (typeof data[0] != 'undefined') {
                                var i = 0;
                                if (data[0].search) {
                                    collection = {};
                                    angular.element("#tpms-temp-chart").empty();
                                    angular.element("#tpms-temp-clickdata").empty();
                                }
                                console.log(data);
                                angular.forEach(data, function (value, key) {
                                    if (!(value.deviceId in collection)) collection[value.deviceId] = {};
                                    if (!(value.tpms in collection[value.deviceId])) collection[value.deviceId][value.tpms] = [];
                                    collection[value.deviceId][value.tpms].push([value.time, value.temp]);
                                    i++;
                                });
                                if (data[0].search && plot != '') {
                                    /*plot.setData(dataForPlot(picked));
                                     plot.setupGrid();
                                     plot.draw();*/
                                    generateChart(picked);
                                }
                                //console.log("aaaadazda " + JSON.stringify(collection));
                            }
                        } else {
                            angular.forEach(data['remove'], function (value) {
                                if (value.id in collection) {
                                    delete collection[value.id];
                                    //console.log("pick " + picked + "value " + value.id);
                                    if (picked == value.id) {
                                        angular.element("#tpms-temp-chart").empty();
                                        angular.element("#tpms-temp-clickdata").empty();
                                    }
                                }
                            });
                        }
                    }
                });

                scope.$watch(attr.tpmsPick, function (data) {
                    if (!isEmpty(data)) {
                        console.log("direc point: " + JSON.stringify(data));
                        picked = data.id;

                        angular.element("#tpms-temp-clickdata").empty();
                        if (data.id in collection) {
                            generateChart(data.id);
                        } else {
                            angular.element("#tpms-temp-chart").empty();
                            angular.element("#tpms-temp-clickdata").append("<span class='temp-text'>Pas de résultats</span>");
                        }
                    }
                });

                function dataForPlot(id) {
                    var arr = [];
                    console.log(JSON.stringify(collection));
                    angular.forEach(collection[id], function (value, key) {
                        var lessFour = key.slice(4);
                        lessFour = lessFour.substring(0, lessFour.length - 4);
                        var splitByTwo = lessFour.match(/.{2}/g);
                        var splitByTwo = lessFour.match(/.{2}/g);
                        var s = '';
                        for (var i = splitByTwo.length - 1; i >= 0; i--) {
                            s += splitByTwo[i];
                        }
                        var newString = s.replace(/^0+/, '');
                        var valArr = [];
                        angular.forEach(value, function (value2) {
                            console.log("val: " + value2[0]);
                            valArr.push([parsedData(value2[0]), value2[1]]);
                        });
                        console.log(valArr);
                        arr.push({'data': valArr, 'label': newString})
                    });
                    return arr;
                }

                function generateChart(id) {
                    plot = $.plot(angular.element("#tpms-temp-chart"),
                        dataForPlot(id), {
                            series: {
                                lines: {
                                    show: true,
                                    lineWidth: 2
                                },
                                points: {show: true},
                                shadowSize: 2
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                tickColor: "#dddddd",
                                borderWidth: 0
                            },
                            legend: {
                                backgroundOpacity: 1.0
                            },
                            xaxis: {
                                mode: "time",
                                timezone: null,
                                autoScaleMargin: 0
                            },
                            yaxis: {autoScaleMargin: 0},
                            colors: ["#FA5833", "#2FABE9"]
                        });

                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 5,
                            border: '1px solid #fdd',
                            padding: '2px',
                            'background-color': '#dfeffc',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }

                    var previousPoint = null;
                    angular.element("#tpms-temp-chart").bind("plothover", function (event, pos, item) {
                        /*$("#x").text(pos.x.toFixed(2));
                         $("#y").text(pos.y.toFixed(2));*/
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                angular.element("#tooltip").remove();
                                /*var x = item.datapoint[0].toFixed(2),
                                 y = item.datapoint[1].toFixed(2);*/

                                showTooltip(item.pageX, item.pageY, generateText(item.datapoint[0].toFixed(2), item.datapoint[1].toFixed(2)));
                            }
                        }
                        else {
                            angular.element("#tooltip").remove();
                            previousPoint = null;
                        }
                    });

                    angular.element("#tpms-temp-chart").bind("plotclick", function (event, pos, item) {
                        if (item) {
                            angular.element("#tpms-temp-clickdata").empty();
                            angular.element("#tpms-temp-clickdata").append(generateText(item.datapoint[0].toFixed(2), item.datapoint[1].toFixed(2)));
                            plot.unhighlight();
                            plot.highlight(item.series, item.datapoint);
                        }
                    });
                }

                function generateText(x, y) {
                    var date = new Date(Math.floor(x));
                    date.setHours(date.getHours() - 2);
                    var str = moment(date).format('DD-MM-YYYY HH:mm:ss');
                    return "<span class='temp-text'>Date:</span><span class='temp-val'>" + str + "</span><br><span class='temp-text'> Température:</span><span class='temp-val'>" + y + "</span>";
                }

                function parsedData(x) {
                    var t = moment.parseZone(x);
                    t = moment(t).add(2, 'h');
                    console.log(Date.UTC(t.year(), t.month(), t.date(), t.hour(), t.minute(), t.second(), t.millisecond()))
                    return Date.UTC(t.year(), t.month(), t.date(), t.hour(), t.minute(), t.second(), t.millisecond());
                }

                function isEmpty(obj) {
                    for (var prop in obj) {
                        if (obj.hasOwnProperty(prop)) return false;
                    }
                    return true;
                }

                /*if ($("#temp-chart").length) {
                 var sin = [], cos = [];
                 for (var i = 0; i < 14; i += 0.5) {
                 sin.push([i, Math.sin(i) / i]);
                 cos.push([i, Math.cos(i)]);
                 }
                 }*/

            }
        }
    });