/**
 * Created by Utilisateur on 21/07/2016.
 */

angular.module('EkolisApp')
    .directive('myParsley', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attr) {
                $(el).parsley();
            }
        };
    });