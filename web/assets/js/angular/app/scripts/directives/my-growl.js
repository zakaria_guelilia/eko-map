/**
 * Created by Utilisateur on 17/06/2016.
 */


angular.module('EkolisApp')
    .directive('myGrowl', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                $(element).on('click', function () {
                    $.gritter.add({
                        // (string | mandatory) the heading of the notification
                        title: 'This is a sticky notice!',
                        // (string | mandatory) the text inside the notification
                        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" style="color:#ccc">magnis dis parturient</a> montes, nascetur ridiculus mus.',
                        // (string | optional) the image to display on the left
                        image: 'img/avatar.jpg',
                        // (bool | optional) if you want it to fade out on its own or just sit there
                        sticky: true,
                        // (int | optional) the time you want it to be alive for before fading out
                        time: '',
                        // (string | optional) the class name you want to apply to that specific message
                        class_name: 'my-sticky-class'
                    });

                })

            }
        };
    });