/**
 * Created by Utilisateur on 2016-06-03.
 */

angular.module('EkolisApp')
    .directive('myMap', function (getFactory) {

        return {
            restrict: 'A',
            replace: true,
            templateUrl: assetsBaseDir + 'js/angular/app/views/directives/my-map.html',
            link: function (scope, element, attr) {
                element.bind(attr.pointsource="points");
                var map = L.map('map').setView([47.2276, 5.2137], 6);
                L.tileLayer('http://{s}.tiles.mapbox.com/v3/ekolis.knm8jp8h/{z}/{x}/{y}.png', {
                    attribution: 'EkoMap, <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                    maxZoom: 19,
                    minZoom: 3
                }).addTo(map);
                angular.element('#map').append('<div id="loader" class="alert alert-info alert-custom alert-address-loading">' +
                    '<div class="remonteLoader"><img src= "' + assetsBaseDir + 'images/gears.gif" /><span>Récupération des données...</span></div>' +
                    '<div class="addressLoader" style="display: none" ><img class="addressLoader" src= "' + assetsBaseDir + 'images/gps-balise.gif"><span>Récupération des adresses...</span></div></div>');

                var semiIcon = L.icon({
                    iconUrl: assetsBaseDir + 'images/semi.png',
                    iconSize: [50, 33], // size of the icon
                    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
                    popupAnchor: [2, -13] // point from which the popup should open relative to the iconAnchor
                });

                var chariotIcon = L.icon({
                    iconUrl: assetsBaseDir + 'images/semi_chariot.png',
                    iconSize: [43, 30], // size of the icon
                    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
                    popupAnchor: [-4, -15] // point from which the popup should open relative to the iconAnchor
                });
                var item = "balise";
                var count = 0;
                var baliseCollection = {};
                var chariotCollection = {};
                var currentCollection = baliseCollection;
                var baliseMarkers = [];
                var chariotMarkers = [];
                var currentMarkers = baliseMarkers;
                var baliseCluster = new L.MarkerClusterGroup({
                    disableClusteringAtZoom: 16, polygonOptions: {
                        fillColor: '#d9edf7',
                        color: '#337ab7',
                        weight: 2.5,
                        opacity: 1.3,
                        fillOpacity: 0.8
                    }
                });
                var chariotCluster = new L.MarkerClusterGroup({
                    disableClusteringAtZoom: 16, polygonOptions: {
                        fillColor: '#d6e9c6',
                        color: '#45930b',
                        weight: 2.5,
                        opacity: 1.3,
                        fillOpacity: 0.8
                    }
                });
                var currentCluster = baliseCluster;

                scope.switchCluster = function () {
                    map.removeLayer(currentCluster);
                    if (currentCluster === baliseCluster) {
                        currentCluster = chariotCluster;
                        baliseMarkers = currentMarkers;
                        currentMarkers = chariotMarkers;
                        baliseCollection = currentCollection;
                        currentCollection = chariotCollection;
                        item = "chariot";
                    } else {
                        currentCluster = baliseCluster;
                        chariotMarkers = currentMarkers;
                        currentMarkers = baliseMarkers;
                        chariotCollection = currentCollection;
                        currentCollection = baliseCollection;
                        item = "balise";
                    }

                    map.addLayer(currentCluster);
                    if (currentMarkers.length > 0) {
                        var group = new L.featureGroup(currentMarkers);
                        map.fitBounds(group.getBounds());
                        map.setZoom(8);
                    }
                };

                function updatePoints(pts) {
                    var pick;
                    var ico;
                    if (pts.img == 1) {
                        pick = chariotIcon;
                        ico = assetsBaseDir + 'images/semi_chariot.png';
                    } else {
                        pick = semiIcon;
                        ico = assetsBaseDir + 'images/semi.png';

                    }
                    var marker = L.marker([pts.lat, pts.lon], {icon: pick});
                    marker.on('mouseover', function (e) {
                        this.openPopup();
                    });
                    marker.on('mouseout', function (e) {
                        this.closePopup();
                    });
                    currentCollection[pts.id].push(marker);
                    currentMarkers.push(marker);
                    currentCluster.addLayer(marker);
                    var length = currentCollection[pts.id].length - 1;
                    var split1 = pts.time.split(' ');
                    var split2 = split1[0].split('-');
                    var tmp = split2[2] + "-" + split2[1] + "-" + split2[0] + " " + split1[1];
                    switch (item) {
                        case 'balise':
                            angular.element("#balise-remonte-list-" + pts.id).append("<button id='marker-" + length + "-" + pts.id + "' class='btn btn-balise-list' type='button'><img src='" + ico + "' class='panel-ico' /><span class='remonte-txt'>" + tmp + "</span></button>");
                            break;
                        case 'chariot':
                            angular.element("#chariot-remonte-list-" + pts.id).append("<button id='marker-" + length + "-" + pts.id + "' class='btn btn-chariot-list' type='button'><img src='" + ico + "' class='panel-ico' /><span class='remonte-txt'>" + tmp + "</span></button>");
                            break;
                    }
                    $("#marker-" + length + "-" + pts.id).on("click", function () {
                        var m = currentCollection[pts.id][length];
                        map.panTo(m.getLatLng());
                        if (m.getPopup()) {
                            if (!m.getPopup()._isOpen) {
                                currentCluster.zoomToShowLayer(m, function () {
                                    m.openPopup();
                                });
                            } else {
                                m.closePopup();
                            }
                        }
                    });

                    getFactory.getAddress(pts.lat, pts.lon).then(function (data) {
                        console.log("address");
                        count--;
                        if (count == 0) {
                            scope.hideLoader();
                        }
                        var popup = L.popup({
                            maxWidth: 400
                        }).setContent('<p class="text-muted popup-custom"><strong>Véhicule:</strong> ' + pts.name + '<br><strong>Date:</strong> ' + tmp + '<br><strong>Adresse:</strong> ' + data.formattedAddress + '</p>');
                        marker.bindPopup(popup);
                    })

                }

                function deletePoints(id) {
                    console.log("DeletePoint: " + id);
                    angular.forEach(currentCollection[id], function (value) {
                        currentCluster.removeLayer(value);
                        var index = currentMarkers.indexOf(value);
                        if (index > -1) currentMarkers.splice(index, 1);
                    });
                    delete currentCollection[id];

                    switch (item) {
                        case 'balise':
                            angular.element("#balise-remonte-list-" + id).remove();
                            break
                        case 'chariot':
                            angular.element("#chariot-remonte-list-" + id).remove();
                            break
                    }
                }

                scope.switch = function (deviceId, name) {
                    switch (item) {
                        case 'balise':
                            if (!(deviceId in currentCollection)) {
                                angular.element('#panel-balise-remonte-body').append("<div id='balise-remonte-list-" + deviceId + "' class='dashboard-list metro remonte-list-custom'><span class='remonte-title remonte-title-balise'>" + name + "</span>");
                                currentCollection[deviceId] = [];
                            }
                            break;
                        case 'chariot':
                            if (!(deviceId in currentCollection)) {
                                angular.element('#panel-chariot-remonte-body').append("<div id='chariot-remonte-list-" + deviceId + "' class='dashboard-list metro remonte-list-custom'><span class='remonte-title remonte-title-chariot'>" + name + "</span>");
                                currentCollection[deviceId] = [];
                            }
                            break;
                    }
                };

                scope.hideLoader = function () {
                    angular.element('#loader').hide("slide", {direction: "up"}, 300, function () {
                        angular.element('.remonteLoader').show();
                        angular.element('.addressLoader').hide();
                    });
                };

                scope.$watch(attr.pointsource, function (data) {
                    if (!isEmpty(data) && !('toDelete' in data)) {
                        if (typeof data[0] != 'undefined') {
                            if ('newSearch' in data[0]) {
                                switch (item) {
                                    case 'balise':
                                        angular.element('#panel-balise-remonte-body').empty();
                                        break;
                                    case 'chariot':
                                        angular.element('#panel-chariot-remonte-body').empty();
                                        break;
                                }
                                count = 0;
                                currentCluster.clearLayers();
                                currentMarkers = [];
                                currentCollection = {};
                            }
                            $(".remonteLoader").hide("slide", {direction: "up"}, 300, function () {
                                $(".addressLoader").show("slide", {direction: "down"}, 300);
                            });
                            count += data.length;
                            console.log("val: " + JSON.stringify(data));

                            angular.forEach(data, function (value, key) {
                                //console.log("mapDirective value: " + JSON.stringify(value));
                                switch (item) {
                                    case 'balise':
                                        scope.switch(value.deviceId, value.name);
                                        break;
                                    case 'chariot':
                                        scope.switch(data[0]['chariotId'], value.name);
                                        break;
                                }
                                var img = 0;
                                if (value.wmbus) {
                                    var a = value.wmbus.toString();
                                    if (value.wmbus.indexOf('id="6f15f40000000202"') > -1) img = 1;
                                }
                                var id = value.deviceId;
                                if (item == "chariot") id = data[0]['chariotId'];

                                if (value.lat && value.lon) {
                                    updatePoints({
                                        "id": id,
                                        "name": value.name,
                                        "lat": value.lat,
                                        "lon": value.lon,
                                        "time": value.time,
                                        "img": img
                                    });
                                } else {
                                    scope.hideLoader();
                                }
                            });

                            map.addLayer(currentCluster);
                            if (currentMarkers.length > 0) {
                                var group = new L.featureGroup(currentMarkers);
                                map.fitBounds(group.getBounds());
                                if (map.getZoom() > 8) map.setZoom(8);
                            }
                        } else scope.hideLoader();

                    } else {
                        if ('toDelete' in data) {
                            console.log("toDelete: " + data);
                            angular.forEach(data.toDelete, function (value) {
                                if (value in currentCollection) {
                                    count -= currentCollection[value].length;
                                    console.log("a " + count);
                                    if (count <= 0) {
                                        console.log("abort");
                                        getFactory.abort();
                                        count = 0;
                                        scope.hideLoader();
                                    }
                                    deletePoints(value);
                                    if (currentMarkers.length > 0) {
                                        var group = new L.featureGroup(currentMarkers);
                                        map.fitBounds(group.getBounds());
                                        if (map.getZoom() > 8) map.setZoom(8);
                                    }
                                }
                            })
                        }
                    }
                });
            }
        };


        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) return false;
            }
            return true;
        }
    });