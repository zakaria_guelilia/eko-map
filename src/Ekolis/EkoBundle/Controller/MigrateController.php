<?php

namespace Ekolis\EkoBundle\Controller;

use Ekolis\EkoBundle\Entity\Beacon\Beacon;
use Ekolis\EkoBundle\Entity\Fleet\Fleet;
use Ekolis\EkoBundle\Entity\Pt1000\Pt1000;
use Ekolis\EkoBundle\Entity\Remonte\Remonte;
use Ekolis\EkoBundle\Entity\SubFleet\SubFleet;
use Ekolis\EkoBundle\Entity\Tpms\Tpms;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MigrateController extends Controller
{
    /**
     * @Route("/postFlotte", name="postFlotte")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postFlotteAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NbUp = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response(); // On initialise la rÃ©ponse
        if ($content = $request->getContent()) { // Test si il y a un contenu
            if ($Fleet = json_decode($content)) { // Test si c'est du contenu JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($Fleet as $dataFleet) {
                    if (isset($dataFleet->fleet)) {
                        $Tot++;
                        $bal = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet')->find($dataFleet->fleet); // On recherche si la balise est dÃ©jÃ  existante
                        if ($bal == null) {
                            $NbNew++;
                            $flotte = new Fleet($dataFleet->fleet);
                            $em->persist($flotte);
                        } else $NbUp++;
                    }
                    $response->setStatusCode(Response::HTTP_ACCEPTED);
                    $response->setContent("Bilan flotte : ajout de $NbNew, mise Ã  jour de $NbUp pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                }
                if (isset($em)) $em->flush();
            } else $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @Route("/postSousFlotte", name="postSousFlotte")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postSousFlotteAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NbUp = 0;
        $NoOk = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response(); // On initialise la rÃ©ponse
        if ($content = $request->getContent()) { // Test si il y a un contenu
            if ($subFleet = json_decode($content)) { // Test si c'est du contenu JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($subFleet as $dataSubFleet) {
                    $Tot++;
                    $flotteObject = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet')->find($dataSubFleet->fleet);
                    if ($flotteObject && isset($dataSubFleet->fleet) && isset($dataSubFleet->subFleet)) {
                        $bal = $this->getDoctrine()->getRepository('EkolisEkoBundle:SubFleet\SubFleet')->find($dataSubFleet->subFleet); // On recherche si la balise est dÃ©jÃ  existante
                        if ($bal) {
                            $NbUp++;
                        } else {
                            $NbNew++;
                            $sousFlotte = new SubFleet($flotteObject, $dataSubFleet->subFleet);
                            $em->persist($sousFlotte);
                        }
                    } else $NoOk++;
                    $response->setStatusCode(Response::HTTP_ACCEPTED);
                    $response->setContent("Bilan sous-flotte : ajout de $NbNew, mise Ã  jour de $NbUp et non ajout de $NoOk pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                }
                if (isset($em)) $em->flush();
            } else $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @Route("/postBeacon", name="postBeacon")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postBeaconAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NbUp = 0;
        $NoOk = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response(); // On initialise la rÃ©ponse
        if ($content = $request->getContent()) { // Test si il y a un contenu
            if ($beacon = json_decode($content)) { // Test si c'est du contenu JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($beacon as $dataBeacon) {
                    $Tot++;
                    $flotteObject = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet')->find($dataBeacon->fleet);
                    if ($dataBeacon->subFleet) $sousFlotteObject = $this->getDoctrine()->getRepository('EkolisEkoBundle:SubFleet\SubFleet')->find($dataBeacon->subFleet);
                    if ($flotteObject && isset($dataBeacon->deviceId) && isset($dataBeacon->name) && isset($dataBeacon->fleet)) { //Test si il y a les bons arguments
                        $bal = $this->getDoctrine()->getRepository('EkolisEkoBundle:Beacon\Beacon')->find($dataBeacon->deviceId); // On recherche si la balise est dÃ©jÃ  existante
                        if ($bal) {
                            $NbUp++;
                            $bal->setName($dataBeacon->name)->setFleet($flotteObject);
                            if ($sousFlotteObject) $bal->setSubFleet($sousFlotteObject);
                            $em->persist($bal);
                        } else {
                            $NbNew++;
                            $balise = new Beacon($dataBeacon->deviceId, $dataBeacon->name, $flotteObject);
                            if ($sousFlotteObject) $balise->setSubFleet($sousFlotteObject);
                            $em->persist($balise);
                        }
                    } else $NoOk++;
                }
                if (isset($em)) $em->flush();
                $response->setContent("Bilan balise : ajout de $NbNew, mise Ã  jour de $NbUp et non ajout de $NoOk pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @Route("/postRemonte", name="postRemonte")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postRemonteAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NoOk = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response();
        if ($content = $request->getContent()) { // Test si il y a du contenu
            if ($dataRemontee = json_decode($content)) { // Test si il y a du JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($dataRemontee as $data) {
                    $Tot++;
                    if (isset($data->device_id) && isset($data->nom) && isset($data->time)) { // Test si il y a au moins les trois arguments qu'il faut
                        $NbNew++;
                        $deviceId = $data->device_id;
                        $date = new \DateTime($data->time);
                        $remonte = new Remonte($data->device_id, $data->nom, $date);
                        if (isset($data->lat)) $remonte->setLat($data->lat);
                        if (isset($data->lon)) $remonte->setLon($data->lon);
                        if (isset($data->alt)) $remonte->setAlt($data->alt);
                        if (isset($data->wakeup)) $remonte->setWakeup($data->wakeup);
                        if (isset($data->wmbus)) $remonte->setWmbus($data->wmbus);
                        $em->persist($remonte);
                    } else
                        $NoOk++;
                }
                if (isset($em)) {
                    $em->flush();
                    $response->setContent("Bilan remontÃ©e(s):  ajout de $NbNew et non ajout de $NoOk pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                }
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else $response->setStatusCode(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @Route("/postPt1000Remonte", name="postPt1000Remonte")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postPt1000RemonteAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NoOk = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response();
        if ($content = $request->getContent()) { // Test si il y a du contenu
            if ($dataRemontee = json_decode($content)) { // Test si il y a du JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($dataRemontee as $data) {
                    $Tot++;
                    if (isset($data->device_id) && isset($data->name) && isset($data->time) && isset($data->pt1000) && isset($data->temp)) { // Test si il y a au moins les trois arguments qu'il faut
                        $NbNew++;
                        $date = new \DateTime($data->time);
                        $remontePt1000 = new Pt1000($data->device_id, $data->name, $date, $data->pt1000, $data->temp);
                        $em->persist($remontePt1000);
                    } else $NoOk++;
                }
                if (isset($em)) {
                    $em->flush();
                    $response->setContent("Bilan remontÃ©e(s) des PT1000 :  ajout de $NbNew et non ajout de $NoOk pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                }
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else $response->setStatusCode(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @Route("/postTpmsRemonte", name="postTpmsRemonte")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postTpmsRemonteAction(Request $request)
    {
        //Initialisation
        $NbNew = 0;
        $NoOk = 0;
        $Tot = 0;
        //Lancement du processus
        $response = new Response();
        if ($content = $request->getContent()) { // Test si il y a du contenu
            if ($dataRemontee = json_decode($content)) { // Test si il y a du JSON
                $em = $this->getDoctrine()->getManager();
                foreach ($dataRemontee as $data) {
                    $Tot++;
                    if (isset($data->device_id) && isset($data->name) && isset($data->time) && isset($data->tpms) && isset($data->pression) && isset($data->temp) && isset($data->codealarm)) { // Test si il y a au moins les trois arguments qu'il faut
                        $NbNew++;
                        $date = new \DateTime($data->time);
                        $remonteTpms = new Tpms($data->device_id, $data->name, $date, $data->tpms, $data->pression, $data->temp, $data->codealarm, $data->cpt07);
                        $em->persist($remonteTpms);
                    } else $NoOk++;
                }
                if (isset($em)) {
                    $em->flush();
                    $response->setContent("Bilan remontÃ©e(s) des TPMS :  ajout de $NbNew et non ajout de $NoOk pour un total de $Tot, le " . date("Y-m-d H:i:s"));
                }
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else $response->setStatusCode(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        } else $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }
}
