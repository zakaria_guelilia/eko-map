<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UserController extends Controller
{
    /**
     * @Route("/getUsers", name="getUsers")
     * @Method("GET")
     * @return Response
     */
    public function getUserAction()
    {
        $response = new Response();
        $userRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Person\User');

        $users = $userRepo->selectUser();
        $response->setContent(json_encode($users));

        return $response;
    }

    /**
     * @Route("/admin/user/delete", name="deleteUser")
     * @Method("POST")
     * @return Response
     */
    public function deleteUserAction(Request $request)
    {
        $post = json_decode($request->getContent());
        $id = $post->id;
        $fleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Person\User');
        $response = new Response();
        $arr = array();

        $fleetRepo->deleteUserById($id) ? array_push($arr, array('msg' => "success")) : array_push($arr, array('msg' => "error"));
        $response->setContent(json_encode($arr));

        return $response;
    }

}
