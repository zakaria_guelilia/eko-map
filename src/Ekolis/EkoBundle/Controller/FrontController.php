<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 2016-05-31
 * Time: 10:31
 */

namespace Ekolis\EkoBundle\Controller;

use Ekolis\EkoBundle\Entity\Beacon\Beacon;
use Ekolis\EkoBundle\Entity\Fleet\Fleet;
use Ekolis\EkoBundle\Entity\SubFleet\SubFleet;
use Ekolis\EkoBundle\Entity\Person\User;
use Ekolis\EkoBundle\Form\Type\BaliseType;
use Ekolis\EkoBundle\Form\Type\FleetType;
use Ekolis\EkoBundle\Form\Type\SubFleetType;
use Ekolis\EkoBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{

    /**
     * @Route("/", name="index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return true;
    }

    /**
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     * @Template("EkolisEkoBundle:Default:login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return array('last_username' => $lastUsername, 'error' => $error);
    }

    /**
     * @Route("/logout", name="logout")
     * @Method({"GET", "POST"})
     */
    public function logoutAction(Request $request)
    {
        return true;
    }

    /**
     * @Route("/map", name="map")
     * @Method("GET")
     * @Template("EkolisEkoBundle:Default:map.html.twig")
     * @return Response
     */
    public function mapAction()
    {
        return true;
    }

    /**
     * @Route("/admin", name="admin")
     * @Method({"GET", "POST"})
     * @Template("EkolisEkoBundle:Default:admin.html.twig")
     */
    public function adminAction(Request $request)
    {
        $roles = $this->container->getParameter('security.role_hierarchy.roles');
        $user = new User();
        $form = $this->createForm(new UserType(array(
            'roles' => $roles
        )), $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Person\User');
            $id = $request->request->get('id');
            $encoder = $this->get('security.password_encoder');
            $id != null ? $userRepo->updateUser($id, $user, $encoder) : $userRepo->createUser($user, $encoder);

            return $this->redirect($this->generateUrl(
                'admin'
            ));
        }

        $fleet = new Fleet();
        $fleetForm = $this->createForm(new FleetType(array()), $fleet);
        $fleetForm->handleRequest($request);
        if ($fleetForm->isSubmitted() && $fleetForm->isValid()) {
            $fleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet');
            $fleetRepo->createFleet($fleet);
            return $this->redirect($this->generateUrl(
                'admin'
            ));
        }

        $subFleet = new SubFleet();
        $subFleetForm = $this->createForm(new SubFleetType(array()), $subFleet);
        $subFleetForm->handleRequest($request);
        if ($subFleetForm->isSubmitted() && $subFleetForm->isValid()) {
            $subFleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:SubFleet\SubFleet');
            $id = $request->request->get('id');
            $id != null ? $subFleetRepo->updateSubFleet($id, $subFleet) : $subFleetRepo->createSubFleet($subFleet);
            return $this->redirect($this->generateUrl(
                'admin'
            ));
        }

        $beacon = new Beacon();
        $beaconForm = $this->createForm(new BaliseType($this->getDoctrine()->getManager()), $beacon);
        $beaconForm->handleRequest($request);
        if ($beaconForm->isSubmitted() && $beaconForm->isValid()) {
            $beaconRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Beacon\Beacon');
            $id = $request->request->get('id');
            $id != null ? $beaconRepo->updateBeacon($id, $beacon) : $beaconRepo->createBeacon($beacon);

            return $this->redirect($this->generateUrl(
                'admin'
            ));
        }

        return array(
            'userForm' => $form->createView(),
            'fleetForm' => $fleetForm->createView(),
            'subFleetForm' => $subFleetForm->createView(),
            'beaconForm' => $beaconForm->createView()
        );
    }
}
