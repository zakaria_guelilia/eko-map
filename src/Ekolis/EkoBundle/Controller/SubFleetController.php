<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SubFleetController extends Controller
{
    /**
     * @Route("/getSubFleets", name="getSubFleets")
     * @Method("GET")
     * @return Response
     */
    public function getSubFleetAction()
    {
        $response = new Response();
        $subFleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:SubFleet\SubFleet');

        $subFleets = $subFleetRepo->selectSubFleet();
        $response->setContent(json_encode($subFleets));

        return $response;
    }

    /**
     * @Route("/admin/subfleet/delete", name="deleteSubFleet")
     * @Method("POST")
     * @return Response
     */
    public function deleteSubFleetAction(Request $request)
    {
        $post = json_decode($request->getContent());
        $id = $post->id;
        $subFleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:SubFleet\SubFleet');
        $response = new Response();
        $arr = array();

        $subFleetRepo->deleteSubFleetById($id) ? array_push($arr, array('msg' => "success")) : array_push($arr, array('msg' => "error"));
        $response->setContent(json_encode($arr));

        return $response;
    }

}
