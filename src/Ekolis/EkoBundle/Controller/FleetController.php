<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FleetController extends Controller
{
    /**
     * @Route("/getFleets", name="getFleets")
     * @Method("GET")
     * @return Response
     */
    public function getFleetAction()
    {
        $response = new Response();
        $fleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet');
        
        $fleets = $fleetRepo->selectFleet();
        $response->setContent(json_encode($fleets));

        return $response;
    }

    /**
     * @Route("/admin/fleet/delete", name="deleteFleet")
     * @Method("POST")
     * @return Response
     */
    public function deleteFleetAction(Request $request)
    {
        $post = json_decode($request->getContent());
        $id = $post->id;
        $fleetRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Fleet\Fleet');
        $response = new Response();
        $arr = array();

        $fleetRepo->deleteFleetById($id) ? array_push($arr, array('msg' => "success")) : array_push($arr, array('msg' => "error"));
        $response->setContent(json_encode($arr));

        return $response;
    }

}
