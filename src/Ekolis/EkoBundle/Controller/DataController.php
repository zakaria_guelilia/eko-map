<?php

namespace Ekolis\EkoBundle\Controller;

use Ekolis\EkoBundle\Controller\BaseCustomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DataController extends BaseCustomController
{
    /**
     * @Route("/getRemonteByBeacon", name="getRemonteByBeacon")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function getRemonteByBeaconAction(Request $request)
    {
        $response = new Response();
        if ($deviceId = $request->query->get('balises')) {
            $remonteRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Remonte\Remonte');
            $data = array();
            
            if ($deviceId == 'last') {
                $result = $remonteRepo->selectLastRemonteData();
                $date = new \DateTime(date("Y-m-d H:i:s"));
		$date->sub(new \DateInterval('P3M'));
		$TimeDeb=$date->format('Y-m-d H:i:s');
                $result ? $data[] = array('time' => $result[0]["time"]->format('Y-m-d H:i:s')) : $data[] = array('time' => $TimeDeb);
                if ($data) $response->setContent(json_encode($data));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else {
            $filter = $request->query->get('filter');
            $filter2 = $request->query->get('filter2');

            if ($filter && $filter2) {
                $data = $remonteRepo->selectRemonteByIntervalByBeacon($filter, $filter2, $deviceId);
            } elseif ($this->dateValidator($filter) && !$filter2) {
                $d = new \DateTime($filter);
                $d2 = new \DateTime($filter);
                $d2->modify('+1 day');
                $data = $remonteRepo->selectRemonteByIntervalByBeacon($d->format('Y-m-d'), $d2->format('Y-m-d'), $deviceId);
            } else $data = $remonteRepo->selectLastRemonteByBeacon($deviceId);
            if ($data) $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_ACCEPTED);
            }
        } else $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        return $response;
    }

    /**
     * @Route("/getRemonteByPt1000/{pt1000}", name="getRemonteByPt1000")
     * @Method("GET")
     * @param Request $request
     * @param $pt1000
     * @return Response
     */
    public function getRemonteByPt1000Action(Request $request, $pt1000)
    {
        $response = new Response();
        $remonteRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Remonte\Remonte');
        $data = array();
        $filter = $request->query->get('filter');
        $filter2 = $request->query->get('filter2');

        if ($filter && $filter2) {
            $data = $remonteRepo->selectRemonteByIntervalByPt1000($filter, $filter2, $pt1000);
        } elseif ($this->dateValidator($filter) && !$filter2) {
            $d = new \DateTime($filter);
            $d2 = new \DateTime($filter);
            $d2->modify('+1 day');
            $data = $remonteRepo->selectRemonteByIntervalByPt1000($d->format('Y-m-d'), $d2->format('Y-m-d'), $pt1000);
        } else $data = $remonteRepo->selectLastRemonteByPt1000($pt1000);
        if ($data) $response->setContent(json_encode($data));
        $response->setStatusCode(Response::HTTP_ACCEPTED);
        return $response;
    }
}
