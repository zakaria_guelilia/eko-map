<?php

namespace Ekolis\EkoBundle\Controller;

use Ekolis\EkoBundle\Controller\BaseCustomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class TpmsController extends BaseCustomController
{
    /**
     * @Route("/getTpmsDataByBeacon", name="getTpmsDataByBeacon")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function getTpmsDataByBeaconAction(Request $request)
    {
        $response = new Response();
        if ($deviceId = $request->query->get('device_id')) {
            $tpmsRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Tpms\Tpms');
            $data = array();

            if ($deviceId == 'last') {
                $result = $tpmsRepo->selectLastTpmsData();
                $date = new \DateTime(date("Y-m-d H:i:s"));
		$date->sub(new \DateInterval('P3M'));
		$TimeDeb=$date->format('Y-m-d H:i:s');
                $result ? $data[] = array('time' => $result[0]["time"]->format('Y-m-d H:i:s')) : $data[] = array('time' => $TimeDeb);
                if ($data) $response->setContent(json_encode($data));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else {
                $filter = $request->query->get('filter');
                $filter2 = $request->query->get('filter2');

                if ($filter && $filter2) {
                    $data = $tpmsRepo->selectTpmsDataByIntervalByBeacon($filter, $filter2, $deviceId);
                } elseif ($this->dateValidator($filter) && !$filter2) {
                    $d = new \DateTime($filter);
                    $d2 = new \DateTime($filter);
                    $d2->modify('+1 day');
                    $data = $tpmsRepo->selectTpmsDataByIntervalByBeacon($d->format('Y-m-d'), $d2->format('Y-m-d'), $deviceId);
                } else $data = $tpmsRepo->selectLastTpmsDataByBeacon($deviceId);
                if ($data) $response->setContent(json_encode($data));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            }
        } else $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        return $response;
    }
}
