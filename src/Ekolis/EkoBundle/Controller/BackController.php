<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class BackController extends Controller
{
    /**
     * @Route("/getAddress", name="getAddress")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function getAddressAction(Request $request)
    {
        $response = new Response();
        $lat = $request->query->get('lat');
        $lon = $request->query->get('lon');

        $key = '3QnspMfaaQf04H885En7~SX9Fs5y07GaL93Co6lfKZQ~AsljURvnLm1C82Rr3QTIwWIFQdaBwMGuRrZzSNVsg8brfF90uOVovrfBAIz27MMg';
        $url = 'http://dev.virtualearth.net/REST/v1/Locations/' . $lat . ',' . $lon . '?key=' . $key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        $data = json_decode($result);

        $response->setContent(json_encode($data->resourceSets[0]->resources[0]->address));

        return $response;
    }
}
