<?php

namespace Ekolis\EkoBundle\Controller;

use Ekolis\EkoBundle\Controller\BaseCustomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class Pt1000Controller extends BaseCustomController
{
    /**
     * @Route("/getPt1000DataByBeacon", name="getPt1000DataByBeacon")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function getPt1000DataByBeaconAction(Request $request)
    {
        $response = new Response();
        if ($deviceId = $request->query->get('device_id')) {
            $pt1000Repo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Pt1000\Pt1000');
            $data = array();

            if ($deviceId == 'last') {
                $result = $pt1000Repo->selectLastPt1000Data();
		$date = new \DateTime(date("Y-m-d H:i:s"));
		$date->sub(new \DateInterval('P3M'));
		$TimeDeb=$date->format('Y-m-d H:i:s');
                $result ? $data[] = array('time' => $result[0]["time"]->format('Y-m-d H:i:s')) : $data[] = array('time' => $TimeDeb);
                if ($data) $response->setContent(json_encode($data));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            } else {
                $filter = $request->query->get('filter');
                $filter2 = $request->query->get('filter2');

                if ($filter && $filter2) {
                    $data = $pt1000Repo->selectPt1000DataByIntervalByBeacon($filter, $filter2, $deviceId);
                } elseif ($this->dateValidator($filter) && !$filter2) {
                    $d = new \DateTime($filter);
                    $d2 = new \DateTime($filter);
                    $d2->modify('+1 day');
                    $data = $pt1000Repo->selectPt1000DataByIntervalByBeacon($d->format('Y-m-d'), $d2->format('Y-m-d'), $deviceId);
                } else $data = $pt1000Repo->selectLastPt1000DataByBeacon($deviceId);
                if ($data) $response->setContent(json_encode($data));
                $response->setStatusCode(Response::HTTP_ACCEPTED);
            }
        } else $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        return $response;
    }
}
