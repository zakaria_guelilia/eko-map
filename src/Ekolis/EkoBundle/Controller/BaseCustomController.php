<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseCustomController extends Controller
{
    protected function dateValidator($s)
    {
        if (preg_match('@^(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)$@', $s, $m) == false) {
            return false;
        }
        return true;
    }
}
