<?php

namespace Ekolis\EkoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class BeaconController extends Controller
{

    /**
     * @Route("/getBeacon", name="getBeacon")
     * @Method("GET")
     * @return Response
     */
    public function getBeaconAction()
    {
        $response = new Response();
        $beaconRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Beacon\Beacon');

        $beacons = $beaconRepo->selectBeacon();
        $response->setContent(json_encode($beacons));

        return $response;
    }

    /**
     * @Route("/getBeaconByFleet/{fleet}", name="getBeaconvByFleet")
     * @Method("GET")
     * @param string fleet
     * @return Response
     */
    public function getBeaconByFleetAction($fleet)
    {
        $response = new Response();
        $beaconRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Beacon\Beacon');

        $beacons = $beaconRepo->selectBeaconByFleet($fleet);
        $response->setContent(json_encode($beacons));

        return $response;
    }

    /**
     * @Route("/admin/beacon/delete", name="deleteBeacon")
     * @Method("POST")
     * @return Response
     */
    public function deleteBeaconAction(Request $request)
    {
        $post = json_decode($request->getContent());
        $id = $post->id;
        $beaconRepo = $this->getDoctrine()->getRepository('EkolisEkoBundle:Beacon\Beacon');
        $response = new Response();
        $arr = array();

        $beaconRepo->deleteBeaconById($id) ? array_push($arr, array('msg' => "success")) : array_push($arr, array('msg' => "error"));
        $response->setContent(json_encode($arr));

        return $response;
    }
}
