<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 17:46
 */

namespace Ekolis\EkoBundle\Entity\Pt1000;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Connection;


class Pt1000Repository extends EntityRepository
{
    public function selectLastPt1000Data()
    {
        $dql = 'SELECT pt1000.time FROM EkolisEkoBundle:Pt1000\Pt1000 pt1000 ORDER BY pt1000.time DESC';
        $query = $this->_em->createQuery($dql)->setMaxResults(1);

        return $query->getResult();

    }

    public function selectLastPt1000DataByBeacon($ids)
    {
        $sql = "SELECT p.device_id as deviceId, name, time, pt1000, temp FROM (SELECT device_id, MAX(time) as MaxTime FROM pt1000 GROUP BY device_id) r INNER JOIN pt1000 as p ON p.device_id = r.device_id AND p.time = r.MaxTime AND p.device_id IN(:ids) GROUP BY p.device_id, p.name";
        $stmt = $this->_em->getConnection()
            ->executeQuery($sql,
                array('ids' => $ids),
                array('ids' => Connection::PARAM_STR_ARRAY)
            );

        return $stmt->fetchAll();

    }

    public function selectPt1000DataByIntervalByBeacon($startDate, $endDate, $ids)
    {
        $dql = "SELECT p FROM EkolisEkoBundle:Pt1000\Pt1000 p WHERE p.deviceId IN (:ids) AND p.time between :date1 AND :date2 ORDER BY p.time ASC";
        $query = $this->_em->createQuery($dql)
            ->setParameter('ids', $ids, Connection::PARAM_STR_ARRAY)
            ->setParameter('date1', $startDate)
            ->setParameter('date2', $endDate);
        $result = $this->dataToJson($query->getResult());

        return $result;
    }

    /**
     * @param $pt1000Data
     * @return array
     */
    private function dataToJson($pt1000Data)
    {
        $data = array();
        foreach ($pt1000Data as $row) {
            $arr = array(
                "deviceId" => $row->getDeviceId(),
                "name" => $row->getName(),
                "time" => $row->getTime()->format('Y-m-d H:i:s'),
                "pt1000" => $row->getPt1000(),
                "temp" => $row->getTemp()
            );

            array_push($data, $arr);
        }
        return $data;
    }
}
