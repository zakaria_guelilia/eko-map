<?php

namespace Ekolis\EkoBundle\Entity\Pt1000;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pt1000
 *
 * @ORM\Table(name="pt1000")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Pt1000\Pt1000Repository")
 */
class Pt1000 {

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_info", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $nbInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=120)
     *
     */
    private $deviceId;

    /**
     * @var text
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var datetime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    private $time;
    
    /**
     * @var text
     *
     * @ORM\Column(name="pt1000", type="text")
     */
    private $pt1000;
    
    /**
     * @var float
     *
     * @ORM\Column(name="temp", type="float")
     */
    private $temp;

    /**
     * @return int
     */
    public function getNbInfo() {
        return $this->nbInfo;
    }

    /**
     * @param int $nbInfo
     */
    public function setNbInfo($nbInfo) {
        $this->nbInfo = $nbInfo;
    }

    /**
     * @return string
     */
    public function getDeviceId() {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return Remonte
     */
    public function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return text
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param text $name
     * @return Remonte
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * @param datetime $time
     * @return Remonte
     */
    public function setTime($time) {
        $this->time = $time;
        return $this;
    }
    
     /**
     * @return datetime
     */
    public function getPt1000() {
        return $this->pt1000;
    }

    /**
     * @param string $namePt1000
     * @return Remonte
     */
    public function setPt1000($pt1000) {
        $this->pt1000 = $pt1000;
        return $this;
    }
    
      /**
     * @return float
     */
    public function getTemp() {
        return $this->temp;
    }

    /**
     * @param float $temp
     * @return Remonte
     */
    public function setTemp($temp) {
        $this->temp = $temp;
        return $this;
    }
    
    public function __construct($deviceId,$name,$time,$pt1000,$temp) {
        $this->setDeviceId($deviceId)->setName($name)->setTime($time)->setPt1000($pt1000)->setTemp($temp);
    }
    
    /**
     * @return tableau
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['deviceId'] = $this->getDeviceId();
        $DataRemonte['name'] = $this->getName();
        $DataRemonte['time'] = $this->getTime()->format('Y-m-d H:i:s');
        $DataRemonte['pt1000'] = $this->getPt1000();
        $DataRemonte['temp'] = $this->getTemp();
        return $DataRemonte;
    }
}
