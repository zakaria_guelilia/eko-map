<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 15:28
 */

namespace Ekolis\EkoBundle\Entity\Person;

use Doctrine\ORM\EntityRepository;


class UserRepository extends EntityRepository
{
    public function createUser($user, $encoder)
    {
        $password = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);
        $parts = explode("/", $user->getRoles());
        if ($parts[count($parts) - 1] === '') array_pop($parts);
        $user->setRoles($parts);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function selectUser()
    {
        $result = $this->findBy([], ['username' => 'ASC']);

        return $this->dataToJson($result);
    }

    public function updateUser($id, $newVal, $encoder)
    {
        $userDB = $this->_em->getRepository('EkolisEkoBundle:Person\User')->find($id);
        if ($userDB) {
            if ($newVal->getUsername() != $userDB->getUsername()) $userDB->setUsername($newVal->getUsername());
            if ($newVal->getEmail() != $userDB->getEmail()) $userDB->setEmail($newVal->getEmail());
            if ($newVal->getPassword() != $userDB->getPassword()) {
                $password = $encoder->encodePassword($newVal, $newVal->getPassword());
                $userDB->setPassword($password);
            }
            if ($newVal->getIsActive() != $userDB->getIsActive()) $userDB->setIsActive($newVal->getIsActive());
            $parts = explode("/", $newVal->getRoles());
            if ($parts[count($parts) - 1] === '') array_pop($parts);
            if ($newVal->getRoles() != $userDB->getRoles()) $userDB->setRoles($parts);
            if ($newVal->getFleet() != $userDB->getFleet()) $userDB->setFleet($newVal->getFleet());
            $this->_em->persist($userDB);
            $this->_em->flush();
        }
    }

    public function deleteUserById($id)
    {
        $user = $this->find($id);
        if (isset($user)) {
            $this->_em->remove($user);
            $this->_em->flush();
            return true;
        }
        return false;
    }


    /**
     * @param $users
     * @return array
     */
    private function dataToJson($users)
    {
        $data = array();
        foreach ($users as $user) {
            $arr = array(
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "password" => $user->getPassword(),
                "email" => $user->getEmail(),
                "isActive" => $user->getIsActive(),
                "roles" => $user->getRoles()
            );
            $user->getFleet() != null ? $arr['fleet'] = $user->getFleet()->getFleet() : $arr['fleet'] = $user->getFleet();

            array_push($data, $arr);
        }
        return $data;
    }
}
