<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 13/06/2016
 * Time: 11:28
 */

namespace Ekolis\EkoBundle\Entity\Person;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Person\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();

    /**
     * @ORM\ManyToOne(targetEntity="Ekolis\EkoBundle\Entity\Fleet\Fleet", inversedBy="fleet")
     * @ORM\JoinColumn(name="fleet", referencedColumnName="fleet", onDelete="SET NULL")
     */
    private $fleet;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }
    
    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getFleet()
    {
        return $this->fleet;
    }

    /**
     * @param mixed $fleet
     */
    public function setFleet($fleet)
    {
        $this->fleet = $fleet;
    }

    public function __construct()
    {
        $this->isActive = true;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->roles
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->roles
            ) = unserialize($serialized);
    }

    /**
     * @return array
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['id'] = $this->getId();
        $DataRemonte['username'] = $this->getUsername();
        $DataRemonte['password'] = $this->getPassword();
        $DataRemonte['email'] = $this->getEmail();
        $DataRemonte['isActive'] = $this->getIsActive();
        $DataRemonte['roles'] = $this->getRoles();
        $this->getFleet() != null ? $DataRemonte['fleet'] = $this->getFleet()->getFleet() : $DataRemonte['fleet'] = $this->getFleet();
        return $DataRemonte;
    }
}
