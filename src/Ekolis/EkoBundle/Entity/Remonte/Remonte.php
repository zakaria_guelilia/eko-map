<?php

namespace Ekolis\EkoBundle\Entity\Remonte;

use Doctrine\ORM\Mapping as ORM;

/**
 * Remonte
 *
 * @ORM\Table(name="remonte")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Remonte\RemonteRepository")
 */
class Remonte {

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_info", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $nbInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=120)
     *
     */
    private $deviceId;

    /**
     * @var text
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var datetime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    private $time;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", nullable=true)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="float", nullable=true)
     */
    private $lon;

    /**
     * @var float
     *
     * @ORM\Column(name="alt", type="float", nullable=true)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="wakeup", type="string", length=120, nullable=true)
     */
    private $wakeup;

    /**
     * @var text
     *
     * @ORM\Column(name="wmbus", type="text", nullable=true)
     */
    private $wmbus;

    /**
     * @return int
     */
    public function getNbInfo() {
        return $this->nbInfo;
    }

    /**
     * @param int $nbInfo
     */
    public function setNbInfo($nbInfo) {
        $this->nbInfo = $nbInfo;
    }

    /**
     * @return string
     */
    public function getDeviceId() {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return Remonte
     */
    public function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return text
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param text $name
     * @return Remonte
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * @param datetime $time
     * @return Remonte
     */
    public function setTime($time) {
        $this->time = $time;
        return $this;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Remonte
     */
    public function setLat($lat) {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat() {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return Remonte
     */
    public function setLon($lon) {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon() {
        return $this->lon;
    }

    /**
     * Set alt
     *
     * @param float $alt
     *
     * @return Remonte
     */
    public function setAlt($alt) {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return float
     */
    public function getAlt() {
        return $this->alt;
    }

    /**
     * @return string
     */
    public function getWakeup() {
        return $this->wakeup;
    }

    /**
     * @param string $wakeup
     * @return Remonte
     */
    public function setWakeup($wakeup) {
        $this->wakeup = $wakeup;
        return $this;
    }

    /**
     * @return text
     */
    public function getWmbus() {
        return $this->wmbus;
    }

    /**
     * @param text $wmbus
     * @return Remonte
     */
    public function setWmbus($wmbus) {
        $this->wmbus = $wmbus;
        return $this;
    }

    public function __construct($deviceId,$name,$time) {
        $this->setDeviceId($deviceId)->setName($name)->setTime($time);
    }
    
    /**
     * @return tableau
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['deviceId'] = $this->getDeviceId();
        $DataRemonte['name'] = $this->getName();
        $DataRemonte['time'] = $this->getTime()->format('Y-m-d H:i:s');
        $DataRemonte['lat'] = $this->getLat();
        $DataRemonte['lon'] = $this->getLon();
        $DataRemonte['alt'] = $this->getAlt();
        $DataRemonte['wakeup'] = $this->getWakeup();
        $DataRemonte['wmbus'] = $this->getWmbus();
        return $DataRemonte;
    }

}
