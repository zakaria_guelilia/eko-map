<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 27/07/2016
 * Time: 09:33
 */

namespace Ekolis\EkoBundle\Entity\Remonte;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Connection;

class RemonteRepository extends EntityRepository
{
    public function selectLastRemonteData()
    {
        $dql = 'SELECT r.time FROM EkolisEkoBundle:Remonte\Remonte r where r.deviceId not in (\'104353\',\'104496\') ORDER BY r.time DESC';
        $query = $this->_em->createQuery($dql)->setMaxResults(1);

        return $query->getResult();

    }
    
    public function selectLastRemonteByBeacon($ids)
    {
        $sql = "SELECT p.device_id as deviceId, name, time, lat, lon, alt, wakeup, wmbus FROM (SELECT device_id, MAX(time) as MaxTime FROM remonte GROUP BY device_id) r INNER JOIN remonte as p ON p.device_id = r.device_id  AND p.time = r.MaxTime AND p.device_id IN (:ids) GROUP BY p.device_id, p.name";
        $stmt = $this->_em->getConnection()
            ->executeQuery($sql,
                array('ids' => $ids),
                array('ids' => Connection::PARAM_STR_ARRAY)
            );

        return $stmt->fetchAll();
    }

    public function selectRemonteByIntervalByBeacon($startDate, $endDate, $ids)
    {
        $dql = "SELECT r FROM EkolisEkoBundle:Remonte\Remonte r WHERE r.deviceId IN (:ids) AND r.time between :date1 AND :date2 ORDER BY r.time ASC";
        $query = $this->_em->createQuery($dql)
            ->setParameter('ids', $ids, Connection::PARAM_STR_ARRAY)
            ->setParameter('date1', $startDate)
            ->setParameter('date2', $endDate);
        $result = $this->dataToJson($query->getResult());

        return $result;
    }

    public function selectLastRemonteByPt1000($pt1000)
    {
        $dql = "SELECT p FROM EkolisEkoBundle:Remonte\Remonte p WHERE  p.wmbus like :pt1000  ORDER BY p.time DESC";
        $query = $this->_em->createQuery($dql)
            ->setParameter('pt1000', '%' . $pt1000 . '%')
            ->setMaxResults(1);
        $result = $query->getSingleResult();
        return $this->dataToJson(array($result));
    }

    public function selectRemonteByIntervalByPt1000($startDate, $endDate, $pt1000)
    {
        $dql = "SELECT p FROM EkolisEkoBundle:Remonte\Remonte p WHERE  p.wmbus like :pt1000 AND p.time between :time1 AND :time2 ORDER BY p.time ASC";
        $query = $this->_em->createQuery($dql)
            ->setParameter('pt1000', '%' . $pt1000 . '%')
            ->setParameter('time1', $startDate)
            ->setParameter('time2', $endDate);
        $result = $query->getResult();

        return $this->dataToJson($result);
    }


    /**
     * @param $remonte
     * @return array
     */
    private function dataToJson($remonte)
    {
        $data = array();
        foreach ($remonte as $row) {
            $arr = array(
                "deviceId" => $row->getDeviceId(),
                "name" => $row->getName(),
                "time" => $row->getTime()->format('Y-m-d H:i:s'),
                "lat" => $row->getLat(),
                "lon" => $row->getLon(),
                "alt" => $row->getAlt(),
                "wakeup" => $row->getWakeup(),
                "wmbus" => $row->getWmbus(),
            );
            array_push($data, $arr);
        }
        return $data;
    }
}
