<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 14:35
 */

namespace Ekolis\EkoBundle\Entity\Beacon;

use Doctrine\ORM\EntityRepository;

class BeaconRepository extends EntityRepository
{
    public function createBeacon($beacon)
    {
        $this->_em->persist($beacon);
        $this->_em->flush();
    }

    public function selectBeacon()
    {
        $result = $this->findBy([], ['fleet' => 'ASC', 'subFleet' => 'ASC']);

        return $this->dataToJson($result);
    }

    public function selectBeaconByFleet($fleet)
    {
        $result = $this->findBy(array('fleet' => $fleet), array('fleet' => 'ASC', 'subFleet' => 'ASC'));

        return $this->dataToJson($result);
    }

    public function updateBeacon($id, $newVal)
    {
        $beaconDB = $this->_em->getRepository('EkolisEkoBundle:Beacon\Beacon')->find($id);
        if ($beaconDB) {
            if ($newVal->getDeviceId() != $beaconDB->getDeviceId()) $beaconDB->setDeviceId($newVal->getDeviceId());
            if ($newVal->getName() != $beaconDB->getName()) $beaconDB->setName($newVal->getName());
            if ($newVal->getFleet() != $beaconDB->getFleet()) $beaconDB->setFleet($newVal->getFleet());
            if ($newVal->getSubFleet() != $beaconDB->getSubFleet()) $beaconDB->setSubFleet($newVal->getSubFleet());
            $this->_em->persist($beaconDB);
            $this->_em->flush();
        }
    }

    public function deleteBeaconById($id)
    {
        $beacon = $this->find($id);
        if (isset($beacon)) {
            $this->_em->remove($beacon);
            $this->_em->flush();
            return true;
        }
        return false;
    }


    /**
     * @param $beacons
     * @return array
     */
    private function dataToJson($beacons)
    {
        $data = array();
        foreach ($beacons as $beacon) {
            $arr = array(
                "deviceId" => $beacon->getDeviceId(),
                "name" => $beacon->getName()
            );
            $beacon->getFleet() != null ? $arr['fleet'] = $beacon->getFleet()->getFleet() : $arr['fleet'] = $beacon->getFleet();
            $beacon->getSubFleet() != null ? $arr['subFleet'] = $beacon->getSubFleet()->getSubFleet() : $arr['subFleet'] = $beacon->getSubFleet();

            array_push($data, $arr);
        }
        return $data;
    }
}
