<?php

namespace Ekolis\EkoBundle\Entity\Beacon;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Balise
 *
 * @ORM\Table(name="balise")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Beacon\BeaconRepository")
 */
class Beacon
{
    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=120)
     * @ORM\Id
     *
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @ManyToOne(targetEntity="Ekolis\EkoBundle\Entity\Fleet\Fleet",inversedBy="fleet")
     * @JoinColumn(name="fleet", referencedColumnName="fleet", nullable=false)
     */
    private $fleet;
    
    /**
     * @ManyToOne(targetEntity="Ekolis\EkoBundle\Entity\SubFleet\SubFleet",inversedBy="subFleet")
     * @JoinColumn(name="subfleet", referencedColumnName="subfleet", nullable=true, onDelete="SET NULL")
     */
    private $subFleet;


    /**
     * Set $device_id
     *
     * @param string $deviceId
     *
     * @return Balise
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }


    /**
     * Get $device_id
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set fleet
     *
     * @param text $fleet
     *
     * @return Balise
     */
    public function setFleet($fleet)
    {
        $this->fleet = $fleet;

        return $this;
    }

    /**
     * Get fleet
     *
     * @return text
     */
    public function getFleet()
    {
        return $this->fleet;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Balise
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;

    }



    /**
     * @return mixed
     */
    public function getSubFleet()
    {
        return $this->subFleet;
    }

    /**
     * @param mixed $subFleet
     */
    public function setSubFleet($subFleet)
    {
        $this->subFleet = $subFleet;
    }
    
    public function __construct($deviceId = null, $name = null, $fleet = null) {
        if (!empty($deviceId)) $this->setDeviceId($deviceId);
        if (!empty($name)) $this->setName($name);
        if (!empty($fleet)) $this->setFleet($fleet);
    }

    /**
     * @return array
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['id'] = $this->getId();
        $DataRemonte['username'] = $this->getUsername();
        $DataRemonte['password'] = $this->getPassword();
        $DataRemonte['email'] = $this->getEmail();
        $DataRemonte['isActive'] = $this->getIsActive();
        $DataRemonte['roles'] = $this->getRoles();
        $this->getFleet() != null ? $DataRemonte['fleet'] = $this->getFleet()->getFleet() : $DataRemonte['fleet'] = $this->getFleet();
        return $DataRemonte;
    }
}
