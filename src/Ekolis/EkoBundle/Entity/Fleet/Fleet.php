<?php

namespace Ekolis\EkoBundle\Entity\Fleet;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fleet
 *
 * @ORM\Table(name="flotte")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Fleet\FleetRepository")
 *
 */
class Fleet {

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="fleet", type="string")
     * @OneToMany(targetEntity="Ekolis\EkoBundle\Entity\Beacon\Beacon", mappedBy="fleet")
     */
    private $fleet;

    /**
     * @ORM\OneToMany(targetEntity="Ekolis\EkoBundle\Entity\Person\User", mappedBy="id")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="Ekolis\EkoBundle\Entity\Beacon\Beacon", mappedBy="deviceId")
     */
    private $balises;

    /**
     * Set fleet
     *
     * @param text $fleet
     *
     * @return Flotte
     */
    public function setFleet($fleet) {
        $this->fleet = $fleet;

        return $this;
    }

    /**
     * Get fleet
     * @return string
     */
    public function getFleet() {
        return $this->fleet;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getBalises()
    {
        return $this->balises;
    }

    /**
     * @param mixed $balises
     */
    public function setBalises($balises)
    {
        $this->balises = $balises;
    }


    public function __construct($fleet = null) {
        if($fleet != null)$this->setFleet($fleet);
        $this->setUsers(new ArrayCollection());
        $this->setBalises(new ArrayCollection());

    }

    public function __toString()
    {
        return $this->getFleet();
    }

    /**
     * @return array
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['fleet'] = $this->getFleet();
        return $DataRemonte;
    }
}
