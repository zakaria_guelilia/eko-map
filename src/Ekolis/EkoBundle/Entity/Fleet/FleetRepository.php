<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 09:36
 */

namespace Ekolis\EkoBundle\Entity\Fleet;

use Doctrine\ORM\EntityRepository;

class FleetRepository extends EntityRepository
{
    public function createFleet($fleet)
    {
        $this->_em->persist($fleet);
        $this->_em->flush();
    }

    public function selectFleet()
    {
        $result = $this->findAll();

        return $this->dataToJson($result);
    }

    public function deleteFleetById($id)
    {
        $fleet = $this->find($id);
        if (isset($fleet)) {
            $this->_em->remove($fleet);
            $this->_em->flush();
            return true;
        }
        return false;
    }


    /**
     * @param $fleets
     * @return array
     */
    private function dataToJson($fleets)
    {
        $data = array();
        foreach ($fleets as $fleet) {
            array_push($data, array('fleet' => $fleet->getFleet()));
        }
        return $data;
    }
}
