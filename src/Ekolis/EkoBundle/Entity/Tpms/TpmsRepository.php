<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 15:51
 */

namespace Ekolis\EkoBundle\Entity\Tpms;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Connection;

class TpmsRepository extends EntityRepository
{
    public function selectLastTpmsData()
    {
        $dql = 'SELECT tpms.time FROM EkolisEkoBundle:Tpms\Tpms tpms ORDER BY tpms.time DESC';
        $query = $this->_em->createQuery($dql)->setMaxResults(1);

        return $query->getResult();

    }

    public function selectLastTpmsDataByBeacon($ids)
    {
        $sql = "SELECT t.device_id as deviceId, name, time, tpms, pression, temp, t.codealarm as codeAlarm, cpt07 FROM (SELECT device_id, MAX(time) as MaxTime FROM tpms GROUP BY device_id) r INNER JOIN tpms as t ON t.device_id = r.device_id  AND t.time = r.MaxTime AND t.device_id IN(:ids) GROUP BY t.device_id, t.name";
        $stmt = $this->_em->getConnection()
            ->executeQuery($sql,
                array('ids' => $ids),
                array('ids' => Connection::PARAM_STR_ARRAY)
            );

        return $stmt->fetchAll();

    }

    public function selectTpmsDataByIntervalByBeacon($startDate, $endDate, $ids)
    {
        $dql = "SELECT p FROM EkolisEkoBundle:Tpms\Tpms p WHERE p.deviceId IN (:ids) AND p.time between :date1 AND :date2 ORDER BY p.time ASC";
        $query = $this->_em->createQuery($dql)
            ->setParameter('ids', $ids, Connection::PARAM_STR_ARRAY)
            ->setParameter('date1', $startDate)
            ->setParameter('date2', $endDate);
        $result = $this->dataToJson($query->getResult());

        return $result;
    }


    /**
     * @param $tpmsData
     * @return array
     */
    private function dataToJson($tpmsData)
    {
        $data = array();
        foreach ($tpmsData as $row) {
            $arr = array(
                "deviceId" => $row->getDeviceId(),
                "name" => $row->getName(),
                "time" => $row->getTime()->format('Y-m-d H:i:s'),
                "tpms" => $row->getTpms(),
                "pression" => $row->getPression(),
                "temp" => $row->getTemp(),
                "codeAlarm" => $row->getCodeAlarm(),
            );

            array_push($data, $arr);
        }
        return $data;
    }
}
