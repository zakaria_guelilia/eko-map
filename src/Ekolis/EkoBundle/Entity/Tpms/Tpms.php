<?php

namespace Ekolis\EkoBundle\Entity\Tpms;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpms
 *
 * @ORM\Table(name="tpms")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\Tpms\TpmsRepository")
 */
class Tpms {

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_info", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $nbInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=120)
     *
     */
    private $deviceId;

    /**
     * @var text
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var datetime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    private $time;
    
    /**
     * @var text
     *
     * @ORM\Column(name="tpms", type="text")
     */
    private $tpms;
    
    /**
     * @var float
     *
     * @ORM\Column(name="pression", type="float")
     */
    private $pression;
    
    /**
     * @var float
     *
     * @ORM\Column(name="temp", type="float")
     */
    private $temp;
    
    /**
     * @var text
     *
     * @ORM\Column(name="codealarm", type="text")
     */
    private $codeAlarm;
    
    /**
     * @var int
     *
     * @ORM\Column(name="cpt07", type="integer", nullable=true)
     */
    private $cpt07;

    /**
     * @return int
     */
    public function getNbInfo() {
        return $this->nbInfo;
    }

    /**
     * @param int $nbInfo
     */
    public function setNbInfo($nbInfo) {
        $this->nbInfo = $nbInfo;
    }

    /**
     * @return string
     */
    public function getDeviceId() {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return Tpms
     */
    public function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return text
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param text $name
     * @return Tpms
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return date
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * @param datetime $time
     * @return Tpms
     */
    public function setTime($time) {
        $this->time = $time;
        return $this;
    }
    
     /**
     * @return datetime
     */
    public function getTpms() {
        return $this->tpms;
    }

    /**
     * @param string $tpms
     * @return Tpms
     */
    public function setTpms($tpms) {
        $this->tpms = $tpms;
        return $this;
    }
    
    /**
     * @return float
     */
    public function getPression() {
        return $this->pression;
    }

    /**
     * @param float $pression
     * @return Tpms
     */
    public function setPression($pression) {
        $this->pression = $pression;
        return $this;
    }
    
    /**
     * @return float
     */
    public function getTemp() {
        return $this->temp;
    }

    /**
     * @param float $temp
     * @return Tpms
     */
    public function setTemp($temp) {
        $this->temp = $temp;
        return $this;
    }
    
     /**
     * @return text
     */
    public function getCodeAlarm() {
        return $this->codeAlarm;
    }

    /**
     * @param string $codeAlarm
     * @return Tpms
     */
    public function setCodeAlarm($codeAlarm) {
        $this->codeAlarm = $codeAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getCpt07()
    {
        return $this->cpt07;
    }

    /**
     * @param int $cpt07
     */
    public function setCpt07($cpt07)
    {
        $this->cpt07 = $cpt07;
    }

    
    public function __construct($deviceId,$name,$time,$tpms,$pression,$temp,$codeAlarm,$cpt07) {
        $this->setDeviceId($deviceId)->setName($name)->setTime($time)->setTpms($tpms)->setPression($pression)->setTemp($temp)->setCodeAlarm($codeAlarm)->setCpt07($cpt07);
    }
    
    /**
     * @return tableau
     */
    public function getDataJson() {
        $DataRemonte = array(); // tableau qui va contenir l'ensemble des data
        $DataRemonte['deviceId'] = $this->getDeviceId();
        $DataRemonte['name'] = $this->getName();
        $DataRemonte['time'] = $this->getTime()->format('Y-m-d H:i:s');
        $DataRemonte['tpms'] = $this->getTpms();
        $DataRemonte['pression'] = $this->getPression();
        $DataRemonte['temp'] = $this->getTemp();
        $DataRemonte['codeAlarm'] = $this->getCodeAlarm();
        return $DataRemonte;
    }
}
