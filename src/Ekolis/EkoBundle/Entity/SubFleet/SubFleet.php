<?php

namespace Ekolis\EkoBundle\Entity\SubFleet;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Ekolis\EkoBundle\Entity\Balise;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fleet
 *
 * @ORM\Table(name="sousflotte")
 * @ORM\Entity(repositoryClass="Ekolis\EkoBundle\Entity\SubFleet\SubFleetRepository")
 */
class SubFleet
{

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="subfleet", type="string")
     * @OneToMany(targetEntity="Ekolis\EkoBundle\Entity\Beacon\Beacon", mappedBy="subFleet")
     */
    private $subFleet;

    /**
     * @ManyToOne(targetEntity="Ekolis\EkoBundle\Entity\Fleet\Fleet",inversedBy="fleet")
     * @JoinColumn(name="fleet", referencedColumnName="fleet", onDelete="CASCADE", nullable=false)
     */
    private $fleet;

    /**
     * @ORM\OneToMany(targetEntity="Ekolis\EkoBundle\Entity\Beacon\Beacon", mappedBy="deviceId")
     */
    private $balises;

    /**
     * @return mixed
     */
    public function getSubFleet()
    {
        return $this->subFleet;
    }

    /**
     * @param mixed $subFleet
     */
    public function setSubFleet($subFleet)
    {
        $this->subFleet = $subFleet;
    }

    /**
     * Get fleet
     *
     * @return text
     */
    public function getFleet()
    {
        return $this->fleet;
    }

    /**
     * Set fleet
     *
     * @param text $fleet
     *
     * @return Balise
     */
    public function setFleet($fleet)
    {
        $this->fleet = $fleet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalises()
    {
        return $this->balises;
    }

    /**
     * @param mixed $balises
     */
    public function setBalises($balises)
    {
        $this->balises = $balises;
    }


    public function __construct($fleet = null, $subFleet = null)
    {
        if ($fleet != null) $this->setFleet($fleet);
        if ($subFleet != null) $this->setSubFleet($subFleet);
        $this->setBalises(new ArrayCollection());
    }

    public function __toString()
    {
        return $this->getSubFleet();
    }

    /**
     * @return array
     */
    public function getDataJson()
    {
        $dataSubFleetJson = array(); // tableau qui va contenir l'ensemble des data
        $dataSubFleetJson['subFleet'] = $this->getSubFleet();
        $dataSubFleetJson['fleet'] = $this->getFleet()->getFleet();

        return $dataSubFleetJson;
    }
}
