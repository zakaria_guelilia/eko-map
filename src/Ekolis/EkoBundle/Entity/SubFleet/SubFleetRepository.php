<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/07/2016
 * Time: 14:13
 */

namespace Ekolis\EkoBundle\Entity\SubFleet;

use Doctrine\ORM\EntityRepository;

class SubFleetRepository extends EntityRepository
{
    public function createSubFleet($subFleet)
    {
        $this->_em->persist($subFleet);
        $this->_em->flush();
    }

    public function selectSubFleet()
    {
        $result = $this->findBy([], ['subFleet' => 'ASC']);

        return $this->dataToJson($result);
    }

    public function updateSubFleet($id, $newVal)
    {
        $subFleetDB = $this->_em->getRepository('EkolisEkoBundle:SubFleet\SubFleet')->find($id);
        if ($subFleetDB) {
            if ($newVal->getSubFleet() != $subFleetDB->getSubFleet()) $subFleetDB->setSubFleet($newVal->getSubFleet());
            if ($newVal->getFleet() != $subFleetDB->getFleet()) $subFleetDB->setFleet($newVal->getFleet());
            $this->_em->persist($subFleetDB);
            $this->_em->flush();
        }
    }

    public function deleteSubFleetById($id)
    {
        $subFleet = $this->find($id);
        if (isset($subFleet)) {
            $this->_em->remove($subFleet);
            $this->_em->flush();
            return true;
        }
        return false;
    }


    /**
     * @param $subFleets
     * @return array
     */
    private function dataToJson($subFleets)
    {
        $data = array();
        foreach ($subFleets as $subFleet) {
            array_push($data, array('subFleet' => $subFleet->getSubFleet(), 'fleet' => $subFleet->getFleet()->getFleet()));
        }
        return $data;
    }
}
