<?php

namespace Ekolis\EkoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    private $roles;
    private $role;

    public function __construct($options = array())
    {
        $roles_choices = array();

        # set roles array, displaying inherited roles between parentheses
        foreach ($options['roles'] as $role => $inherited_roles) {
            foreach ($inherited_roles as $id => $inherited_role) {
                if (!array_key_exists($inherited_role, $roles_choices)) {
                    $roles_choices[$inherited_role] = $inherited_role;
                }
            }

            if (!array_key_exists($role, $roles_choices)) {
                $s = $role . '/' . implode('/', $inherited_roles) . '/';
                if (substr($s, -1) == '/') $s = rtrim($s, "/");
                $roles_choices[$s] = $s;
            }
        }


        $this->roles = $roles_choices;
        $this->role = $role;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arr = [];
        $builder
            ->add('username', 'text', array('label' => "Nom d'utilisateur"))
            ->add('password', 'password', $arr)
            ->add('email', 'email')
            ->add('isActive', 'choice', array(
                'choices' => array(
                    'Oui' => 1,
                    'Non' => 0,
                ),
                'choices_as_values' => true))
            ->add('roles', 'choice', array(
                'choices' => $this->roles,
                'data' => $this->role,
            ))
            ->add('fleet', 'entity', array(
                    'required' => false,
                    'class' => 'Ekolis\EkoBundle\Entity\Fleet\Fleet',
                    'label' => 'Flotte'
                )
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'user';
    }
}
