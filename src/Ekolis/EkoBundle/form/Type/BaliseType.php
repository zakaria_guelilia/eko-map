<?php

namespace Ekolis\EkoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BaliseType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('deviceId', 'text', array('label' => "Device Id"))
            ->add('name', 'text', array('label' => "Nom"))
            ->add('fleet', 'entity', array(
                    'required' => true,
                    'class' => 'Ekolis\EkoBundle\Entity\Fleet\Fleet',
                    'label' => 'Flotte',
                    'placeholder' => 'Choisissez une flotte'
                )
            )
            ->add('subFleet', 'entity', array(
                    'required' => false,
                    'class' => 'Ekolis\EkoBundle\Entity\SubFleet\SubFleet',
                    'label' => 'Sous-flotte'
                )
            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'balise';
    }
}
