<?php

namespace Ekolis\EkoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubFleetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subfleet', 'text', array('label' => "Nom de sous-flotte"))
            ->add('fleet', 'entity', array(
                    'class' => 'Ekolis\EkoBundle\Entity\Fleet\Fleet',
                    'label' => 'Flotte',
                    'placeholder' => 'Choisissez une flotte'
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'sub_fleet';
    }
}
