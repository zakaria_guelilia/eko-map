module.exports = function(grunt) {  
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //resourcesPath: 'src/MyBundle/Resources',
        watch: {

            css: {
                files: 'web/assets/css/**/*.css',
                tasks: [],
                options: {
                    livereload: true
                }
            },
            js: {
                files: 'web/assets/js/**/*.js',
                tasks: [],
                options: {
                    livereload: true
                }
            },
            twig: {
                files: 'src/Ekolis/EkoBundle/Resources/views/**/*.twig',
                tasks: [],
                options: {
                    livereload: true
                }
            }
        }

    });

    // Load the plugin that provides the "watch" task.
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch']);
    
};