# Ekolis Manager v2 #
----------------------------------

# Changelog #
----------------------------------

## **[0.3.3]** - *2016-08-12*
### Added
* Enregistrement des courbe pt1000 / tpms au format PDF
* Recherche d'une balise

## **[0.3.2]** - *2016-08-11*
### Added
* Ajout de fonctions pour repositories TPMS / Remonte
### Modified
* Fix date dernière remontée / remontée tpms

## **[0.3.1]** - *2016-08-10*
### Modified
* Fix le setName de BeaconEntity

## **[0.3.0]** - *2016-08-10*
### Added
* Ajout de repositories pour chaque entité
### Modified
* Passage de 2 controllers à un controller par entité
* Les modals formulaire possèdent leurs propres templates
* Factorisation du code
### SensioLab Fix
#### Major
* Sensitive data should not be present in non-parameter configuration files
* Public methods in controller classes should only be actions
* Source code should not contain FIXME comments
* The EntityManager should not be flushed within a loop
* Symfony applications should not contain a config.php file

#### Minor
* Source code should not contain TODO comments
* Commented code should not be committed
* Unused use statement should be avoided
* Form types should be in Form/Type folders

#### Info
* Default favicon should be changed
* Text files should end with a newline character
* Project files should not mix end of lines


## **[0.2.0]** - *2016-07-25*
### Added
* Gestion Balise (CRUD)
* Gestion Flotte (CRUD)
* Gestion Sous-flotte (CRUD)
* Affichage des courbes de température TPMS
### Modified
* Nom de la directive "my-pressure" to "my-tpms-pressure"

## **[0.1.0]** - *2016-07-13*
### Added
* Création du model (Balise - Flotte - Sous-Flotte - pt1000 - Remonte - Tpms - User)
* Gestion utilisateur (CRUD)
* Gestion authentification
* Affichage liste des balises
* Gestion multi-selection de balise
* Création des filtres de recherche
* Affichage de la map avec gestion des clusters
* Affichage des remontées
* Affichage des courbes de température pt1000
* Affichage des courbes de pression TPMS
* Gestion du double affichage balise/chariot
* Affichage des loaders


# Installing the project #
----------------------------------
**Coming soon**